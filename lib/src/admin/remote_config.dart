part of '../../admin.dart';

class RemoteConfig extends JsObjectWrapper<rc_interop.RemoteConfigJsImpl>
    implements _RemoteConfig {
  static final _expando = Expando<RemoteConfig>();

  RemoteConfig._fromJsObject(rc_interop.RemoteConfigJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  App get app => App.getInstance(jsObject.app);

  @override
  RemoteConfigTemplate createTemplateFromJson(Map<String, dynamic> json) =>
      RemoteConfigTemplate.getInstance(
          jsObject.createTemplateFromJSON(jsonEncode(json)));

  @override
  Future<RemoteConfigTemplate> getTemplate() =>
      handleThenable(jsObject.getTemplate())
          .then(RemoteConfigTemplate.getInstance);

  @override
  Future<RemoteConfigTemplate> publishTemplate(RemoteConfigTemplate template,
      [rc_interop.PublishOptions options]) async {
    rc_interop.RemoteConfigTemplateJsImpl result;
    if (options == null) {
      result =
          await handleThenable(jsObject.publishTemplate(template.jsObject));
    } else {
      result = await handleThenable(
          jsObject.publishTemplate(template.jsObject, options));
    }
    return RemoteConfigTemplate.getInstance(result);
  }

  @override
  Future<RemoteConfigTemplate> validateTemplate(
          RemoteConfigTemplate template) =>
      handleThenable(jsObject.validateTemplate(template.jsObject))
          .then(RemoteConfigTemplate.getInstance);

  static RemoteConfig getInstance(rc_interop.RemoteConfigJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= RemoteConfig._fromJsObject(jsObject);
  }
}

class RemoteConfigParameter
    extends JsObjectWrapper<rc_interop.RemoteConfigParameterJsImpl>
    implements _RemoteConfigParameter {
  static final _expando = Expando<RemoteConfigParameter>();

  factory RemoteConfigParameter({
    Map<String, dynamic> conditionalValues,
    rc_interop.ExplicitParameterValue defaultValue,
    String description,
  }) =>
      RemoteConfigParameter._fromJsObject(
          rc_interop.RemoteConfigParameterJsImpl(
        conditionalValues: jsify(conditionalValues),
        defaultValue: defaultValue,
        description: description,
      ));

  RemoteConfigParameter._fromJsObject(
      rc_interop.RemoteConfigParameterJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Map<String, dynamic> get conditionalValues {
    if (jsObject.conditionalValues == null) return null;
    return dartify(jsObject.conditionalValues);
  }

  @override
  set conditionalValues(Map<String, dynamic> v) {
    jsObject.conditionalValues = jsify(v);
  }

  @override
  rc_interop.ExplicitParameterValue get defaultValue => jsObject.defaultValue;

  @override
  set defaultValue(rc_interop.ExplicitParameterValue _defaultValue) {
    jsObject.defaultValue = _defaultValue;
  }

  @override
  String get description => jsObject.description;

  @override
  set description(String v) => jsObject.description = v;

  static RemoteConfigParameter getInstance(
      rc_interop.RemoteConfigParameterJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= RemoteConfigParameter._fromJsObject(jsObject);
  }
}

class RemoteConfigParameterGroup
    extends JsObjectWrapper<rc_interop.RemoteConfigParameterGroupJsImpl>
    implements _RemoteConfigParameterGroup {
  static final _expando = Expando<RemoteConfigParameterGroup>();

  factory RemoteConfigParameterGroup({
    String description,
    Map<String, RemoteConfigParameter> parameters,
  }) =>
      RemoteConfigParameterGroup._fromJsObject(
          rc_interop.RemoteConfigParameterGroupJsImpl(
        description: description,
        parameters: jsify(parameters),
      ));

  RemoteConfigParameterGroup._fromJsObject(
      rc_interop.RemoteConfigParameterGroupJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get description => jsObject.description;

  @override
  set description(String v) => jsObject.description = v;

  @override
  Map<String, RemoteConfigParameter> get parameters {
    if (jsObject.parameters == null) return null;
    final groups = dartify(dartifyMap(jsObject.parameters)) as Map;
    return groups.cast<String, RemoteConfigParameter>();
  }

  @override
  set parameters(Map<String, RemoteConfigParameter> v) {
    jsObject.parameters = jsify(v);
  }

  static RemoteConfigParameterGroup getInstance(
      rc_interop.RemoteConfigParameterGroupJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        RemoteConfigParameterGroup._fromJsObject(jsObject);
  }
}

class RemoteConfigTemplate
    extends JsObjectWrapper<rc_interop.RemoteConfigTemplateJsImpl>
    implements _RemoteConfigTemplate {
  static final _expando = Expando<RemoteConfigTemplate>();

  RemoteConfigTemplate._fromJsObject(
      rc_interop.RemoteConfigTemplateJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  List<rc_interop.RemoteConfigCondition> get conditions =>
      List.from(jsObject.conditions);

  @override
  set conditions(List<rc_interop.RemoteConfigCondition> v) {
    jsObject.conditions = v;
  }

  @override
  String get etag => jsObject.etag;

  @override
  set etag(String v) => jsObject.etag = v;

  @override
  Map<String, RemoteConfigParameterGroup> get parameterGroups =>
      dartifyMapWithMapper(
        jsObject.parameterGroups,
        // ignore: unnecessary_lambdas
        (o) => RemoteConfigParameterGroup.getInstance(o),
      );

  @override
  set parameterGroups(Map<String, RemoteConfigParameterGroup> v) =>
      jsObject.parameterGroups = jsify(v);

  @override
  Map<String, RemoteConfigParameter> get parameters => dartifyMapWithMapper(
        jsObject.parameters,
        // ignore: unnecessary_lambdas
        (o) => RemoteConfigParameter.getInstance(o),
      );

  @override
  set parameters(Map<String, RemoteConfigParameter> v) {
    jsObject.parameters = jsify(v);
  }

  static RemoteConfigTemplate getInstance(
      rc_interop.RemoteConfigTemplateJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= RemoteConfigTemplate._fromJsObject(jsObject);
  }
}

abstract class _RemoteConfig {
  App get app;
  RemoteConfigTemplate createTemplateFromJson(Map<String, dynamic> json);
  Future<RemoteConfigTemplate> getTemplate();
  Future<RemoteConfigTemplate> publishTemplate(RemoteConfigTemplate template,
      [rc_interop.PublishOptions options]);
  Future<RemoteConfigTemplate> validateTemplate(RemoteConfigTemplate template);
}

abstract class _RemoteConfigParameter {
  Map<String, dynamic> conditionalValues;
  rc_interop.ExplicitParameterValue defaultValue;
  String description;
}

abstract class _RemoteConfigParameterGroup {
  String description;
  Map<String, RemoteConfigParameter> parameters;
}

abstract class _RemoteConfigTemplate {
  List<rc_interop.RemoteConfigCondition> conditions;
  String etag;
  Map<String, RemoteConfigParameterGroup> parameterGroups;
  Map<String, RemoteConfigParameter> parameters;
}
