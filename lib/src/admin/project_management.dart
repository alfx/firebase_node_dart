part of '../../admin.dart';

class AndroidApp extends JsObjectWrapper<pm_interop.AndroidAppJsImpl>
    implements _AndroidApp {
  static final _expando = Expando<AndroidApp>();

  AndroidApp._fromJsObject(pm_interop.AndroidAppJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get appId => jsObject.appId;

  @override
  Future<void> addShaCertificate(pm_interop.ShaCertificate certificate) =>
      handleThenable(jsObject.addShaCertificate(certificate));

  @override
  Future<void> deleteShaCertificate(pm_interop.ShaCertificate certificate) =>
      handleThenable(jsObject.deleteShaCertificate(certificate));

  @override
  Future<String> getConfig() => handleThenable(jsObject.getConfig());

  @override
  Future<pm_interop.IosAppMetadata> getMetadata() =>
      handleThenable(jsObject.getMetadata());

  @override
  Future<pm_interop.ShaCertificate> getShaCertificates() =>
      handleThenable(jsObject.getShaCertificates());

  @override
  Future<void> setDisplayName(String newDisplayName) =>
      handleThenable(jsObject.setDisplayName(newDisplayName));

  static AndroidApp getInsance(pm_interop.AndroidAppJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= AndroidApp._fromJsObject(jsObject);
  }
}

class IosApp extends JsObjectWrapper<pm_interop.IosAppJsImpl>
    implements _IosApp {
  static final _expando = Expando<IosApp>();

  IosApp._fromJsObject(pm_interop.IosAppJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get appId => jsObject.appId;

  @override
  Future<String> getConfig() => handleThenable(jsObject.getConfig());

  @override
  Future<pm_interop.IosAppMetadata> getMetadata() =>
      handleThenable(jsObject.getMetadata());

  @override
  Future<void> setDisplayName(String newDisplayName) =>
      handleThenable(jsObject.setDisplayName(newDisplayName));

  static IosApp getInsance(pm_interop.IosAppJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= IosApp._fromJsObject(jsObject);
  }
}

class ProjectManagement
    extends JsObjectWrapper<pm_interop.ProjectManagementJsImpl>
    implements _ProjectManagement {
  static final _expando = Expando<ProjectManagement>();

  ProjectManagement._fromJsObject(pm_interop.ProjectManagementJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  AndroidApp androidApp(String appId) =>
      AndroidApp.getInsance(jsObject.androidApp(appId));

  @override
  Future<AndroidApp> createAndroidApp(String packageName, String displayName) =>
      handleThenable(jsObject.createAndroidApp(packageName, displayName))
          .then(AndroidApp.getInsance);

  @override
  Future<IosApp> createIosApp(String bundleId, String displayName) =>
      handleThenable(jsObject.createIosApp(bundleId, displayName))
          .then(IosApp.getInsance);

  @override
  IosApp iosApp(String appId) => IosApp.getInsance(jsObject.iosApp(appId));

  @override
  Future<List<AndroidApp>> listAndroidApps() =>
      handleThenable(jsObject.listAndroidApps()).then((value) =>
          List<pm_interop.AndroidAppJsImpl>.from(value)
              .map<AndroidApp>(AndroidApp.getInsance)
              .toList());

  @override
  Future<List<pm_interop.AppMetadata>> listAppMetadata() async {
    final result = await handleThenable(jsObject.listAppMetadata());
    return result.cast<pm_interop.AppMetadata>();
  }

  @override
  Future<List<IosApp>> listIosApps() =>
      handleThenable(jsObject.listAndroidApps()).then((value) =>
          List<pm_interop.IosAppJsImpl>.from(value)
              .map(IosApp.getInsance)
              .toList());

  @override
  Future<void> setDisplayName(String newDisplayName) =>
      handleThenable(jsObject.setDisplayName(newDisplayName));

  @override
  pm_interop.ShaCertificate shaCertificate(String shaHash) =>
      jsObject.shaCertificate(shaHash);

  static ProjectManagement getInstance(
      pm_interop.ProjectManagementJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ProjectManagement._fromJsObject(jsObject);
  }
}

abstract class _AndroidApp {
  String get appId;
  Future<void> addShaCertificate(pm_interop.ShaCertificate certificate);
  Future<void> deleteShaCertificate(pm_interop.ShaCertificate certificate);
  Future<String> getConfig();
  Future<pm_interop.IosAppMetadata> getMetadata();
  Future<pm_interop.ShaCertificate> getShaCertificates();
  Future<void> setDisplayName(String newDisplayName);
}

abstract class _IosApp {
  String get appId;
  Future<String> getConfig();
  Future<pm_interop.IosAppMetadata> getMetadata();
  Future<void> setDisplayName(String newDisplayName);
}

abstract class _ProjectManagement {
  AndroidApp androidApp(String appId);
  Future<AndroidApp> createAndroidApp(String packageName, String displayName);
  Future<IosApp> createIosApp(String bundleId, String displayName);
  IosApp iosApp(String appId);
  Future<List<AndroidApp>> listAndroidApps();
  Future<List<pm_interop.AppMetadata>> listAppMetadata();
  Future<List<IosApp>> listIosApps();
  Future<void> setDisplayName(String newDisplayName);
  pm_interop.ShaCertificate shaCertificate(String shaHash);
}
