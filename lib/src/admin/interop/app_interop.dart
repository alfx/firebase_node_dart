// ignore_for_file: public_member_api_docs
@JS('firebaseAdmin.app')
library firebase_node.interop.app;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';
import 'admin_interop.dart';
import 'auth_interop.dart';
import 'database_interop.dart';
import 'firestore_interop.dart';
import 'instance_id_interop.dart';
import 'machine_learning_interop.dart';
import 'messaging_interop.dart';
import 'project_management_interop.dart';
import 'remote_config_interop.dart';
import 'security_rules_interop.dart';

@JS('App')
abstract class AppJsImpl {
  external String get name;
  external AppOptionsJsImpl get options;
  external AuthJsImpl auth();
  external DatabaseJsImpl database();
  external PromiseJsImpl<void> delete();
  external FirestoreJsImpl firestore();
  external InstanceIdJsImpl instanceId();
  external MachineLearningJsImpl machineLearning();
  external MessagingJsImpl messaging();
  external ProjectManagementJsImpl projectManagement();
  external RemoteConfigJsImpl remoteConfig();
  external SecurityRulesJsImpl securityRules();
}
