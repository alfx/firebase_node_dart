// ignore_for_file: public_member_api_docs
@JS('firebaseAdmin')
library firebase_node.interop.admin;

import 'package:js/js.dart';
import 'package:node_interop/http.dart';

import 'app_interop.dart';
import 'auth_interop.dart';
import 'credential_interop.dart';
import 'database_interop.dart';
import 'firestore_interop.dart';
import 'instance_id_interop.dart';
import 'machine_learning_interop.dart';
import 'messaging_interop.dart';
import 'project_management_interop.dart';
import 'remote_config_interop.dart';
import 'storage_interop.dart';

@JS()
external List /*AppJsImpl*/ get apps;

/// The current SDK version.
///
/// See: <https://firebase.google.com/docs/reference/js/firebase#.SDK_VERSION>.
@JS()
// ignore: non_constant_identifier_names
external String get SDK_VERSION;

@JS()
external AppJsImpl app([String name]);

@JS()
external AuthJsImpl auth([AppJsImpl app]);

@JS()
external DatabaseJsImpl database([AppJsImpl app]);

@JS()
external FirestoreJsImpl firestore([AppJsImpl app]);

@JS()
external AppJsImpl initializeApp(AppOptionsJsImpl options, [String name]);

@JS()
external InstanceIdJsImpl instanceId([AppJsImpl app]);

@JS()
external MachineLearningJsImpl machineLearning([AppJsImpl app]);

@JS()
external MessagingJsImpl messaging([AppJsImpl app]);

@JS()
external ProjectManagementJsImpl projectManagement([AppJsImpl app]);

@JS()
external RemoteConfigJsImpl remoteConfig([AppJsImpl app]);

@JS()
external StorageJsImpl storage([AppJsImpl app]);

@JS('AppOptions')
@anonymous
abstract class AppOptionsJsImpl {
  external factory AppOptionsJsImpl({
    CredentialJsImpl credential,
    dynamic databaseAuthVariableOverride,
    String databaseURL,
    String serviceAccountId,
    String storageBucket,
    String projectId,
    HttpAgent httpAgent,
  });

  external CredentialJsImpl get credential;
  external set credential(CredentialJsImpl v);

  external dynamic get databaseAuthVariableOverride;
  external set databaseAuthVariableOverride(dynamic v);

  external String get databaseURL;
  external set databaseURL(String v);

  external HttpAgent get httpAgent;
  external set httpAgent(HttpAgent v);

  external String get projectId;
  external set projectId(String v);

  external String get serviceAccountId;
  external set serviceAccountId(String v);

  external String get storageBucket;
  external set storageBucket(String v);
}

@JS()
@anonymous
abstract class FirebaseArrayIndexError {
  external FirebaseErrorJsImpl get error;
  external num get index;
}

/// FirebaseError is a subclass of the standard Error object.
/// In addition to a message string, it contains a string-valued code.
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.FirebaseError>.
@JS()
@anonymous
abstract class FirebaseErrorJsImpl {
  external String get code;
  external String get message;
  external String get stack;
}

@JS()
@anonymous
abstract class GoogleOAuthAccessToken {
  @JS('access_token')
  external String get accessToken;
  @JS('expires_in')
  external String get expiresIn;
}

@JS()
@anonymous
abstract class ServiceAccount {
  external factory ServiceAccount({
    String clientEmail,
    String privateKey,
    String projectId,
  });
  external String get clientEmail;
  external set clientEmail(String v);
  external String get privateKey;
  external set privateKey(String v);
  external String get projectId;
  external set projectId(String v);
}
