// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.admin.interop.instance_id;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';
import 'app_interop.dart';

@JS('InstanceId')
@anonymous
abstract class InstanceIdJsImpl {
  external AppJsImpl get app;
  external PromiseJsImpl<void> deleteInstanceId(String instanceId);
}
