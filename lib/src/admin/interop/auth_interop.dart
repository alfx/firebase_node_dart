// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.admin.interop.auth;

import 'package:js/js.dart';
import 'package:meta/meta.dart';

import '../../utils/es6_interop.dart';
import 'admin_interop.dart';

///This is the interface that defines the required continue/state URL with
///optional Android and iOS bundle identifiers.
@JS()
@anonymous
abstract class ActionCodeSettings {
  ///This is the interface that defines the required continue/state URL with
  ///optional Android and iOS bundle identifiers.
  external factory ActionCodeSettings({
    AndroidSettings android,
    String dynamicLinkDomain,
    bool handleCodeInApp,
    IosSettings iOS,
    String url,
  });

  ///Defines the Android package name. This will try to open the link in an
  ///android app if it is installed. If installApp is passed, it specifies
  ///whether to install the Android app if the device supports it and the app
  ///is not already installed. If this field is provided without a packageName,
  ///an error is thrown explaining that the packageName must be provided in
  ///conjunction with this field. If minimumVersion is specified, and an older
  ///version of the app is installed, the user is taken to the Play Store to
  ///upgrade the app.
  external AndroidSettings get android;
  external set android(AndroidSettings v);

  ///Defines the dynamic link domain to use for the current link if it is to be
  ///opened using Firebase Dynamic Links, as multiple dynamic link domains can
  ///be configured per project. This field provides the ability to explicitly
  ///choose configured per project. This fields provides the ability explicitly
  ///choose one. If none is provided, the oldest domain is used by default.
  external String get dynamicLinkDomain;
  external set dynamicLinkDomain(String v);

  ///Whether to open the link via a mobile app or a browser. The default is
  ///false. When set to true, the action code link is sent as a Universal Link
  ///or Android App Link and is opened by the app if installed. In the false
  ///case, the code is sent to the web widget first and then redirects to the
  ///app if installed.
  external bool get handleCodeInApp;
  external set handleCodeInApp(bool v);

  ///Defines the iOS bundle ID. This will try to open the link in an iOS app if
  ///it is installed.
  external IosSettings get iOS;
  external set iOS(IosSettings v);

  ///Defines the link continue/state URL, which has different meanings in
  ///different contexts:
  ///
  ///When the link is handled in the web action widgets, this is the deep link
  ///in the `continueUrl` query parameter
  ///
  ///When the link is handled in the app directly, this is the `continueUrl`
  ///query parameter in the deep link of the Dynamic Link.
  external String get url;
  external set url(String v);
}

@JS()
@anonymous
class AndroidSettings {
  external factory AndroidSettings({
    String packageName,
    String minimumVersion,
    bool installApp,
  });
  external bool get installApp;
  external set installApp(bool b);
  external String get minimumVersion;
  external set minimumVersion(String s);
  external String get packageName;
  external set packageName(String s);
}

@JS()
@anonymous
abstract class AuthJsImpl {
  external PromiseJsImpl<String> createCustomToken(String uid,
      [dynamic developerClaims]);

  external PromiseJsImpl<AuthProviderConfig> createProviderConfig(
      AuthProviderConfig config);

  external PromiseJsImpl<String> createSessionCookie(
      String idToken, SessionCookieOptions sessionCookieOptions);

  external PromiseJsImpl<UserRecordJsImpl> createUser(CreateRequest properties);

  external PromiseJsImpl deleteProviderConfig(String providerId);

  external PromiseJsImpl deleteUser(String uid);

  external PromiseJsImpl<String> generateEmailVerificationLink(String email,
      [ActionCodeSettings actionCodeSettings]);

  external PromiseJsImpl<String> generatePasswordResetLink(String email,
      [ActionCodeSettings actionCodeSettings]);

  external PromiseJsImpl<String> generateSignInWithEmailLink(String email,
      [ActionCodeSettings actionCodeSettings]);

  external PromiseJsImpl<AuthProviderConfig> getProviderConfig(
      String providerId);

  external PromiseJsImpl<UserRecordJsImpl> getUser(String uid);

  external PromiseJsImpl<UserRecordJsImpl> getUserByEmail(String email);

  external PromiseJsImpl<UserRecordJsImpl> getUserByPhoneNumber(
      String phoneNumber);

  external PromiseJsImpl<UsersResult> importUsers(List<UserImportRecord> users,
      [UserImportOptions options]);

  external PromiseJsImpl<ListProviderConfigResults> listProviderConfigs(
      AuthProviderConfigFilter options);

  external PromiseJsImpl<ListUsersResult> listUsers(
      [num maxResults, String pageToken]);

  external PromiseJsImpl<void> revokeRefreshTokens(String uid);

  external PromiseJsImpl<void> setCustomUserClaims(
      String uid, dynamic customUserClaims);

  external TenantManagerJsImpl tenantManager();

  external PromiseJsImpl<AuthProviderConfig> updateProviderConfig(
      String providerId, dynamic updatedConfig);

  external PromiseJsImpl<UserRecordJsImpl> updateUser(
      String uid, UpdateRequest properties);

  // ignore: avoid_positional_boolean_parameters
  external PromiseJsImpl verifyIdToken(String idToken, [bool checkRevoked]);

  external PromiseJsImpl verifySessionCookie(String sessionCookie,
      // ignore: avoid_positional_boolean_parameters
      [bool checkForRevocation]);
}

///The base Auth provider configuration interface.
///
///Parameters
///
///- `displayName`:
///The user-friendly display name to the current configuration. This name is
///also used as the provider label in the Cloud Console.
///
///- `enabled`:
///The user-friendly display name to the current configuration. This name is
///also used as the provider label in the Cloud Console.
///
///- `providerId`:
///The user-friendly display name to the current configuration. This name is
///also used as the provider label in the Cloud Console.
@JS()
@anonymous
abstract class AuthProviderConfig {
  ///The base Auth provider configuration interface.
  ///
  ///Parameters
  ///
  ///- `displayName`:
  ///The user-friendly display name to the current configuration. This name is
  ///also used as the provider label in the Cloud Console.
  ///
  ///- `enabled`:
  ///The user-friendly display name to the current configuration. This name is
  ///also used as the provider label in the Cloud Console.
  ///
  ///- `providerId`:
  ///The user-friendly display name to the current configuration. This name is
  ///also used as the provider label in the Cloud Console.
  external factory AuthProviderConfig({
    String displayName,
    String enabled,
    String providerId,
  });

  ///The user-friendly display name to the current configuration. This name is
  ///also used as the provider label in the Cloud Console.
  external String get displayName;
  external set displayName(String v);

  ///The user-friendly display name to the current configuration. This name is
  ///also used as the provider label in the Cloud Console.
  external bool get enabled;
  external set enabled(bool v);

  ///The user-friendly display name to the current configuration. This name is
  ///also used as the provider label in the Cloud Console.
  external String get providerId;
  external set providerId(String v);
}

///The filter interface used for listing provider configurations. This is used
///when specifying how to list configured identity providers via
///listProviderConfigs().
@JS()
@anonymous
abstract class AuthProviderConfigFilter {
  external factory AuthProviderConfigFilter({
    num maxResults,
    String pageToken,
    /* "saml" | "oidc" */ String type,
  });

  ///The maximum number of results to return per page. The default and maximum
  ///is 100.
  external num get maxResults;
  external set maxResults(num v);

  ///The next page token. When not specified, the lookup starts from the
  ///beginning of the list.
  external String get pageToken;
  external set pageToken(String v);

  ///The Auth provider configuration filter. This can be either saml or oidc.
  ///The former is used to look up SAML providers only, while the latter is
  ///used for OIDC providers.
  external String get type;
  external set type(String v);
}

///Interface representing base properties of a user enrolled second factor for
///a CreateRequest.
@JS()
@anonymous
abstract class CreateMultiFactorInfoRequest {
  external factory CreateMultiFactorInfoRequest({
    String displayName,
    String factorId,
  });

  ///The optional display name for an enrolled second factor.
  external String get displayName;
  external set displayName(String v);

  ///The type identifier of the second factor. For SMS second factors, this is
  ///phone.
  external String get factorId;
  external set factorId(String v);
}

///Interface representing a phone specific user enrolled second factor for a
///CreateRequest.
@JS()
@anonymous
abstract class CreatePhoneMultiFactorInfoRequest {
  external factory CreatePhoneMultiFactorInfoRequest({
    String displayName,
    String factorId,
    String phoneNumber,
  });

  ///The optional display name for an enrolled second factor.
  external String get displayName;
  external set displayName(String v);

  ///The type identifier of the second factor. For SMS second factors, this is
  ///phone.
  external String get factorId;
  external set factorId(String v);

  ///The phone number associated with a phone second factor.
  external String get phoneNumber;
  external set phoneNumber(String v);
}

///Interface representing the properties to set on a new user record to be
///created.
@JS()
@anonymous
abstract class CreateRequest {
  external factory CreateRequest({
    bool disabled,
    String displayName,
    String email,
    bool emailVerified,
    MultiFactorCreateSettings multiFactor,
    String password,
    String phoneNumber,
    String photoURL,
    String uid,
  });

  ///Whether or not the user is disabled: true for disabled; false for enabled.
  external bool get disabled;
  external set disabled(bool v);

  ///The user's display name.
  external String get displayName;
  external set displayName(String v);

  ///The user's primary email.
  external String get email;
  external set email(String v);

  ///Whether or not the user's primary email is verified.
  external bool get emailVerified;
  external set emailVerified(bool v);

  ///The user's multi-factor related properties.
  external MultiFactorCreateSettings get multiFactor;
  external set multiFactor(MultiFactorCreateSettings v);

  ///The user's unhashed password.
  external String get password;
  external set password(String v);

  ///The user's primary phone number.
  external String get phoneNumber;
  external set phoneNumber(String v);

  //The user's photo URL.
  external String get photoURL;
  external set photoURL(String v);

  ///The user's uid.
  external String get uid;
  external set uid(String v);
}

///Interface representing the properties to set on a new tenant.
@JS()
@anonymous
abstract class CreateTenantRequest {
  external factory CreateTenantRequest({
    ///The tenant display name.
    String displayName,
    EmailSignInConfig emailSignInConfig,
  });

  ///The tenant display name.
  external String get displayName;
  external set displayName(String v);

  ///The email sign in configuration.
  external EmailSignInConfig get emailSignInConfig;
  external set emailSignInConfig(EmailSignInConfig v);
}

@JS()
@anonymous
abstract class EmailSignInConfig {
  external factory EmailSignInConfig({
    @required bool enabled,
    bool passwordRequired,
  });

  ///Whether email provider is enabled.
  external bool get enabled;
  external set enabled(bool v);

  ///Whether password is required for email sign-in. When not required, email
  ///sign-in can be performed with password or via email link sign-in.
  external bool get passwordRequired;
  external set passwordRequired(bool v);
}

///The password hashing information.
@JS()
@anonymous
abstract class Hash {
  ///The password hashing information.
  ///
  ///- `algorithm` The password hashing algorithm identifier. The following
  ///algorithm
  ///identifiers are supported: SCRYPT, STANDARD_SCRYPT, HMAC_SHA512,
  ///HMAC_SHA256, HMAC_SHA1, HMAC_MD5, MD5, PBKDF_SHA1, BCRYPT, PBKDF2_SHA256,
  ///SHA512, SHA256, SHA1.
  ///
  ///- Optional blockSize
  ///The block size (normally 8) of the hashing algorithm. Required for the
  ///STANDARD_SCRYPT algorithm.
  ///
  ///- Optional derivedKeyLength
  ///The derived key length of the hashing algorithm. Required for the
  ///STANDARD_SCRYPT algorithm.
  ///
  ///- Optional key Buffer
  ///The signing key used in the hash algorithm in buffer bytes. Required by
  ///hashing algorithms SCRYPT, HMAC_SHA512, HMAC_SHA256, HAMC_SHA1 and
  ///HMAC_MD5.
  ///
  ///- Optional memoryCost
  ///The memory cost required for SCRYPT algorithm, or the CPU/memory cost.
  ///Required for STANDARD_SCRYPT algorithm.
  ///
  ///- Optional parallelization
  ///The parallelization of the hashing algorithm. Required for the
  ///STANDARD_SCRYPT algorithm.
  ///
  ///- Optional rounds
  ///The number of rounds for hashing calculation. Required for SCRYPT, MD5,
  ///SHA512, SHA256, SHA1, PBKDF_SHA1 and PBKDF2_SHA256.
  ///
  ///- Optional saltSeparator Buffer
  ///The salt separator in buffer bytes which is appended to salt when verifying
  ///a password. This is only used by the SCRYPT algorithm.
  external factory Hash({
    @required String algorithm,
    num blockSize,
    num derivedKeyLength,
    String key,
    num memoryCost,
    num parallelization,
    num rounds,
    String saltSeparator,
  });

  external String get algorithm;
  external set algorithm(String v);
  external num get blockSize;

  external set blockSize(num v);
  external num get derivedKeyLength;

  external set derivedKeyLength(num v);
  external String get key;

  external set key(String v);
  external num get memoryCost;

  external set memoryCost(num v);
  external num get parallelization;

  external set parallelization(num v);
  external num get rounds;

  external set rounds(num v);
  external String get saltSeparator;

  external set saltSeparator(String v);
}

@JS()
@anonymous
class IosSettings {
  external factory IosSettings({String bundleId});
  external String get bundleId;
  external set bundleId(String s);
}

///The response interface for listing provider configs. This is only available
///when listing all identity providers' configurations via listProviderConfigs()
///.
@JS()
@anonymous
abstract class ListProviderConfigResults {
  ///The next page token, if available.
  external String get pageToken;
  external set pageToken(String v);

  ///The list of providers for the specified type in the current page.
  external List<AuthProviderConfig> get providerConfigs;
  external set providerConfigs(List<AuthProviderConfig> v);
}

///Interface representing the object returned from a listTenants() operation.
///Contains the list of tenants for the current batch and the next page token
///if available.
@JS()
@anonymous
abstract class ListTenantsResult {
  ///The next page token if available. This is needed for the next batch
  ///download.
  external String get pageToken;
  external set pageToken(String v);

  ///The list of Tenant objects for the downloaded batch.
  external List<Tenant> get tenants;
  external set tenants(List<Tenant> v);
}

@JS()
@anonymous
abstract class ListUsersResult {
  external String get pageToken;
  external List<UserRecordJsImpl> get users;
}

@JS()
@anonymous
abstract class MultiFactorCreateSettings {
  external factory MultiFactorCreateSettings({
    List<CreateMultiFactorInfoRequest> enrolledFactors,
  });
  external List<CreateMultiFactorInfoRequest> get enrolledFactors;
  external set enrolledFactors(List<CreateMultiFactorInfoRequest> v);
}

///Interface representing the common properties of a user enrolled second
///factor.
@JS()
@anonymous
abstract class MultiFactorInfo {
  ///The optional display name of the enrolled second factor.
  external String get displayName;
  external set displayName(String v);

  ///The optional date the second factor was enrolled, formatted as a UTC
  ///string.
  external String get enrollmentTime;
  external set enrollmentTime(String v);

  ///The type identifier of the second factor. For SMS second factors, this is
  ///phone.
  external String get factorId;
  external set factorId(String v);

  ///The ID of the enrolled second factor. This ID is unique to the user.
  external String get uid;
  external set uid(String v);
}

@JS()
@anonymous
abstract class MultiFactorSettings {
  external List<MultiFactorInfo> get enrolledFactors;
  external set enrolledFactors(List<MultiFactorInfo> v);
}

@JS()
@anonymous
abstract class MultiFactorUpdateSettings {
  external List<UpdateMultiFactorInfoRequest> get enrolledFactors;
  external set enrolledFactors(List<UpdateMultiFactorInfoRequest> v);
}

@JS()
@anonymous
abstract class OIDCAuthProviderConfig {
  external String get clientId;
  external set clientId(String v);

  external String get displayName;
  external set displayName(String v);

  external bool get enabled;
  external set enabled(bool v);

  external String get issuer;
  external set issuer(String v);

  external String get providerId;
  external set providerId(String v);
}

@JS()
@anonymous
abstract class OIDCUpdateAuthProviderRequest {
  external String get clientId;
  external set clientId(String v);

  external String get displayName;
  external set displayName(String v);

  external bool get enabled;
  external set enabled(bool v);

  external String get issuer;
  external set issuer(String v);
}

@JS()
@anonymous
abstract class PhoneMultiFactorInfo {
  external String get displayName;
  external set displayName(String v);

  external String get enrollmentTime;
  external set enrollmentTime(String v);

  external String get factorId;
  external set factorId(String v);

  external String get phoneNumber;
  external set phoneNumber(String v);

  external String get uid;
  external set uid(String v);
}

@JS()
@anonymous
abstract class SAMLAuthProviderConfig {
  external String get callbackURL;
  external set callbackURL(String v);

  external String get displayName;
  external set displayName(String v);

  external bool get enabled;
  external set enabled(bool v);

  external String get idpEntityId;
  external set idpEntityId(String v);

  external String get providerId;
  external set providerId(String v);

  external String get rpEntityId;
  external set rpEntityId(String v);

  external String get ssoURL;
  external set ssoURL(String v);

  external String get x509Certificates;
  external set x509Certificates(String v);
}

@JS()
@anonymous
abstract class SAMLUpdateAuthProviderRequest {
  external String get callbackURL;
  external set callbackURL(String v);

  external String get displayName;
  external set displayName(String v);

  external bool get enabled;
  external set enabled(bool v);

  external String get idpEntityId;
  external set idpEntityId(String v);

  external String get rpEntityId;
  external set rpEntityId(String v);

  external String get ssoURL;
  external set ssoURL(String v);

  external String get x509Certificates;
  external set x509Certificates(String v);
}

@JS()
@anonymous
abstract class SessionCookieOptions {
  external factory SessionCookieOptions({num expiresIn});
  external num get expiresIn;
  external set expiresIn(num v);
}

///Interface representing a tenant configuration.
///
///Multi-tenancy support requires Google Cloud's Identity Platform (GCIP). To
///learn more about GCIP, including pricing and features, see the GCIP
///documentation
///
///Before multi-tenancy can be used on a Google Cloud Identity Platform
///project, tenants must be allowed on that project via the Cloud Console UI.
///
///A tenant configuration provides information such as the display name, tenant
///identifier and email authentication configuration. For OIDC/SAML provider
///configuration management, TenantAwareAuth instances should be used instead
///of a Tenant to retrieve the list of configured IdPs on a tenant. When
///configuring these providers, note that tenants will inherit whitelisted
///domains and authenticated redirect URIs of their parent project.
///
///All other settings of a tenant will also be inherited. These will need to be
///managed from the Cloud Console UI.
@JS()
@anonymous
abstract class Tenant {
  external factory Tenant({
    String displayName,
    EmailSignInConfig emailSignInConfig,
    @required String tenantId,
  });

  ///The tenant display name.
  external String get displayName;
  external set displayName(String v);

  ///The email sign in provider configuration.
  external EmailSignInConfig get emailSignInConfig;
  external set emailSignInConfig(EmailSignInConfig v);

  ///The tenant identifier.
  external String get tenantId;
  external set tenantId(String v);
}

@JS('TenantAwareAuth')
@anonymous
abstract class TenantAwareAuthJsImpl {
  String get tenantId;

  external PromiseJsImpl<String> createCustomToken(
    String uid, [
    dynamic developerClaims,
  ]);

  external PromiseJsImpl<AuthProviderConfig> createProviderConfig(
      AuthProviderConfig config);

  external PromiseJsImpl<String> createSessionCookie(
      String idToken, SessionCookieOptions sessionCookieOptions);

  external PromiseJsImpl<UserRecordJsImpl> createUser(CreateRequest properties);

  external PromiseJsImpl<void> deleteProviderConfig(String providerId);

  external PromiseJsImpl<void> deleteUser(String uid);

  external PromiseJsImpl<UsersResult> deleteUsers(List<String> uids);

  external PromiseJsImpl<String> generateEmailVerificationLink(String email,
      [ActionCodeSettings actionCodeSettings]);

  external PromiseJsImpl<String> generatePasswordResetLink(String email,
      [ActionCodeSettings actionCodeSettings]);

  external PromiseJsImpl<String> generateSignInWithEmailLink(String email,
      [ActionCodeSettings actionCodeSettings]);

  external PromiseJsImpl<AuthProviderConfig> getProviderConfig(
      String providerid);

  external PromiseJsImpl<UserRecordJsImpl> getUser(String uid);

  external PromiseJsImpl<GetUsersResultJsImpl> getUsers(dynamic identifiers);

  external PromiseJsImpl<UserRecordJsImpl> getUserByEmail(String email);

  external PromiseJsImpl<UserRecordJsImpl> getUserByPhoneNumber(
      String phoneNumber);

  external PromiseJsImpl<UsersResult> importUsers(List<UserImportRecord> users,
      [UserImportOptions options]);

  external PromiseJsImpl<ListProviderConfigResults> listProviderConfigs(
      AuthProviderConfigFilter options);

  external PromiseJsImpl<ListUsersResult> listUsers(
      [num maxResults, String pageToken]);

  external PromiseJsImpl<void> revokeRefreshTokens(String uid);

  external PromiseJsImpl<void> setCustomUserClaims(
      String uid, dynamic customUserClaims);

  external PromiseJsImpl<AuthProviderConfig> updateProviderConfig(
      String providerId, AuthProviderConfig updatedConfig);

  external PromiseJsImpl<UserRecordJsImpl> updateUser(
      String uid, UpdateRequest properties);

  // ignore: avoid_positional_boolean_parameters
  external PromiseJsImpl verifyIdToken(String idToken, [bool checkRevoked]);

  external PromiseJsImpl verifySessionCookie(String idToken,
      // ignore: avoid_positional_boolean_parameters
      [bool checkRevoked]);
}

@JS('GetUsersResult')
@anonymous
abstract class GetUsersResultJsImpl {
  external List get notFound;
  external List<UserRecordJsImpl> get users;
}

@JS('TenantManager')
@anonymous
abstract class TenantManagerJsImpl {
  external TenantAwareAuthJsImpl authForTenant(String tenantId);

  external PromiseJsImpl<Tenant> createTenant(
      CreateTenantRequest tenantOptions);

  external PromiseJsImpl<void> deleteTenant(String tenantId);

  external PromiseJsImpl<Tenant> getTenant(String tenantId);

  external PromiseJsImpl<ListTenantsResult> listTenants(
      [num maxResults, String pageToken]);

  external PromiseJsImpl<Tenant> updateTenant(
      String tenantId, UpdateRequest tenantOptions);
}

///Interface representing common properties of a user enrolled second factor
///for an UpdateRequest.
@JS()
@anonymous
abstract class UpdateMultiFactorInfoRequest {
  ///Interface representing common properties of a user enrolled second factor
  ///for an UpdateRequest.
  ///
  /// - Optional `displayName`
  /// The optional display name for an enrolled second factor.
  ///
  /// - Optional `enrollmentTime`
  /// The optional date the second factor was enrolled, formatted as a UTC
  /// string.
  ///
  /// - `factorId`
  /// The type identifier of the second factor. For SMS second factors, this is
  /// phone.
  ///
  /// - Optional `uid`
  /// The ID of the enrolled second factor. This ID is unique to the user. When
  /// not provided, a new one is provisioned by the Auth server.
  external factory UpdateMultiFactorInfoRequest({
    String displayName,
    String enrollmentTime,
    @required String factorId,
    String uid,
  });
  external String get displayName;
  external set displayName(String v);

  external String get enrollmentTime;
  external set enrollmentTime(String v);

  external String get factorId;
  external set factorId(String v);

  external String get uid;
  external set uid(String v);
}

///Interface representing a phone specific user enrolled second factor for an
///UpdateRequest.
@JS()
@anonymous
abstract class UpdatePhoneMultiFactorInfoRequest {
  /// Interface representing a phone specific user enrolled second factor for an
  /// UpdateRequest.
  /// - Optional `displayName`
  /// The optional display name for an enrolled second factor.
  ///
  /// - Optional `enrollmentTime`
  /// The optional date the second factor was enrolled, formatted as a UTC
  /// string.
  ///
  /// - `factorId`
  /// The type identifier of the second factor. For SMS second factors, this is
  /// phone.
  ///
  /// - `phoneNumber`
  /// The phone number associated with a phone second factor.
  ///
  /// - Optional `uid`
  /// The ID of the enrolled second factor. This ID is unique to the user. When
  /// not provided, a new one is provisioned by the Auth server.
  external factory UpdatePhoneMultiFactorInfoRequest({
    String displayName,
    String enrollmentTime,
    String factorId,
    String phoneNumber,
    String uid,
  });

  external String get displayName;
  external set displayName(String v);
  external String get enrollmentTime;
  external set enrollmentTime(String v);
  external String get factorId;
  external set factorId(String v);
  external String get phoneNumber;
  external set phoneNumber(String v);
  external String get uid;
  external set uid(String v);
}

/// Interface representing the properties to update on the provided user.
@JS()
@anonymous
abstract class UpdateRequest {
  /// Interface representing the properties to update on the provided user.
  /// - Optional `disabled`
  /// Whether or not the user is disabled: true for disabled; false for enabled.
  ///
  /// - Optional `displayName`
  /// The user's display name.
  ///
  /// - Optional `email`
  /// The user's primary email.
  ///
  /// - Optional `emailVerified`
  /// Whether or not the user's primary email is verified.
  ///
  /// - Optional `multiFactor`
  /// The user's updated multi-factor related properties.
  ///
  /// - Optional `password`
  /// The user's unhashed password.
  ///
  /// - Optional `phoneNumber`
  /// The user's primary phone number.
  ///
  /// - Optional `photoURL`
  /// The user's photo URL.
  external factory UpdateRequest({
    bool disabled,
    String displayName,
    String email,
    bool emailVerified,
    MultiFactorCreateSettings multiFactor,
    String passwordSalt,
    String phoneNumber,
    String photoURL,
  });
  external bool get disabled;
  external set disabled(bool v);
  external String get displayName;
  external set displayName(String v);
  external String get email;
  external set email(String v);
  external bool get emailVerified;
  external set emailVerified(bool v);
  external MultiFactorCreateSettings get multiFactor;
  external set multiFactor(MultiFactorCreateSettings v);
  external String get passwordSalt;
  external set passwordSalt(String v);
  external String get phoneNumber;
  external set phoneNumber(String v);
  external String get photoURL;
  external set photoURL(String v);
}

/// Interface representing the properties to update on the provided tenant.
@JS()
@anonymous
abstract class UpdateTenantRequest {
  /// Interface representing the properties to update on the provided tenant.
  /// - Optional `displayName`
  /// The tenant display name.
  ///
  /// - Optional `emailSignInConfig`
  /// The email sign in configuration.
  external factory UpdateTenantRequest({
    String displayName,
    EmailSignInConfig mailSignInConfig,
  });
  external String get displayName;
  external set displayName(String v);
  external EmailSignInConfig get mailSignInConfig;
  external set mailSignInConfig(EmailSignInConfig v);
}

///Interface representing the user import options needed for importUsers()
///method. This is used to provide the password hashing algorithm information.
@JS()
@anonymous
abstract class UserImportOptions {
  external factory UserImportOptions({Hash hash});

  ///The password hashing information.
  external Hash get hash;
  external set hash(Hash v);
}

/// Interface representing a user to import to Firebase Auth via the
/// importUsers() method.
@JS()
@anonymous
abstract class UserImportRecord {
  /// Interface representing a user to import to Firebase Auth via the
  /// importUsers() method.
  /// Optional customClaims
  /// customClaims: undefined | object
  /// The user's custom claims object if available, typically used to define
  /// user roles and propagated to an authenticated user's ID token.
  ///
  /// - Optional `disabled`
  /// Whether or not the user is disabled: true for disabled; false for enabled.
  ///
  /// - Optional `displayName`
  /// The user's display name.
  ///
  /// - Optional `email`
  /// The user's primary email, if set.
  ///
  /// - Optional `emailVerified`
  /// emailVerified: undefined | false | true
  /// Whether or not the user's primary email is verified.
  ///
  /// - Optional `metadata`
  /// Additional metadata about the user.
  ///
  /// - Optional `multiFactor`
  /// The user's multi-factor related properties.
  ///
  /// - Optional `passwordHash`
  /// The buffer of bytes representing the user's hashed password. When a user
  /// is to be imported with a password hash, UserImportOptions are required to
  /// be specified to identify the hashing algorithm used to generate this hash.
  ///
  /// - Optional `passwordSalt`
  /// The buffer of bytes representing the user's password salt.
  ///
  /// - Optional `phoneNumber`
  /// The user's primary phone number, if set.
  ///
  /// - Optional `photoURL`
  /// The user's photo URL.
  ///
  /// - Optional `providerData`
  /// An array of providers (for example, Google, Facebook) linked to the user.
  ///
  /// - Optional `tenantId`
  /// The identifier of the tenant where user is to be imported to. When not
  /// provided in an admin.auth.Auth context, the user is uploaded to the
  /// default parent project. When not provided in an admin.auth.
  /// TenantAwareAuth context, the user is uploaded to the tenant corresponding
  /// to that TenantAwareAuth instance's tenant ID.
  ///
  /// - `uid`
  /// The user's uid.
  external factory UserImportRecord({
    dynamic customClaims,
    bool disabled,
    String displayName,
    String email,
    bool emailVerified,
    UserMetadata metadata,
    List<MultiFactorUpdateSettings> multiFactor,
    String passwordHash,
    String passwordSalt,
    String phoneNumber,
    String photoURL,
    List<UserProviderRequest> providerData,
    String tenantId,
    @required String uid,
  });

  external dynamic get customClaims;
  external set customClaims(dynamic v);

  external bool get disabled;
  external set disabled(bool v);

  external String get displayName;
  external set displayName(String v);

  external String get email;
  external set email(String v);

  external bool get emailVerified;
  external set emailVerified(bool v);

  external UserMetadata get metadata;
  external set metadata(UserMetadata v);

  external List<MultiFactorUpdateSettings> get multiFactor;
  external set multiFactor(List<MultiFactorUpdateSettings> v);

  external String get passwordHash;
  external set passwordHash(String v);

  external String get passwordSalt;
  external set passwordSalt(String v);

  external String get phoneNumber;
  external set phoneNumber(String v);

  external String get photoURL;
  external set photoURL(String v);

  external List<UserProviderRequest> get providerData;
  external set providerData(List<UserProviderRequest> v);

  external String get tenantId;
  external set tenantId(String v);

  external String get uid;
  external set uid(String v);
}

///Interface representing the response from the importUsers() method for batch
///importing users to Firebase Auth.
@JS()
@anonymous
abstract class UsersResult {
  ///An array of errors corresponding to the provided users to import. The
  ///length of this array is equal to failureCount.
  external List<FirebaseArrayIndexError> get errors;

  ///The number of user records that failed to import to Firebase Auth.
  external num get failureCount;

  ///The number of user records that successfully imported to Firebase Auth.
  external num get successCount;
}

///Interface representing a user's info from a third-party identity provider
///such as Google or Facebook.
@JS()
@anonymous
abstract class UserInfo {
  ///The display name for the linked provider.
  external String get displayName;

  ///The email for the linked provider.
  external String get email;

  ///The phone number for the linked provider.
  external String get phoneNumber;

  ///The photo URL for the linked provider.
  external String get photoURL;

  ///The linked provider ID (for example, "google.com" for the Google provider).
  external String get providerId;

  ///The user identifier for the linked provider.
  external String get uid;
}

///Interface representing a user's metadata.
@JS()
@anonymous
abstract class UserMetadata {
  ///The date the user was created, formatted as a UTC string.
  external String get creationTime;

  ///The date the user was created, formatted as a UTC string.
  external String get lastSignInTime;
}

///User metadata to include when importing a user.
@JS()
@anonymous
abstract class UserMetadataRequest {
  /// User metadata to include when importing a user.
  ///
  /// - Optional `creationTime`
  /// The date the user was created, formatted as a UTC string.
  ///
  /// - Optional `lastRefreshTime`
  /// The time at which the user was last active (ID token refreshed),
  /// formatted as a UTC Date string (eg 'Sat, 03 Feb 2001 04:05:06 GMT'). Null
  /// implies the user was never active.
  ///
  /// - Optional `lastSignInTime`
  /// The date the user last signed in, formatted as a UTC string.
  external factory UserMetadataRequest({
    String creationTime,
    String lastSignInTime,
  });

  /// The time at which the user was last active (ID token refreshed),
  /// formatted as a UTC Date string (eg 'Sat, 03 Feb 2001 04:05:06 GMT'). Null
  /// implies the user was never active.
  external String get creationTime;

  /// The date the user last signed in, formatted as a UTC string.
  external String get lastSignInTime;
}

/// User provider data to include when importing a user.
@JS()
@anonymous
abstract class UserProviderRequest {
  /// User provider data to include when importing a user.
  /// Optional `displayName`
  /// The display name for the linked provider.
  ///
  /// Optional `email`
  /// The email for the linked provider.
  ///
  /// Optional `phoneNumber`
  /// The phone number for the linked provider.
  ///
  /// Optional `photoURL`
  /// The photo URL for the linked provider.
  ///
  /// `providerId`
  /// The linked provider ID (for example, "google.com" for the
  /// Google provider).
  ///
  /// `uid`
  /// The user identifier for the linked provider.
  external factory UserProviderRequest({
    String displayName,
    String email,
    String phoneNumber,
    String photoURL,
    String providerId,
    @required String uid,
  });

  /// The display name for the linked provider.
  external String get displayName;

  /// The display name for the linked provider.
  external set displayName(String v);

  /// The email for the linked provider.
  external String get email;

  /// The email for the linked provider.
  external set email(String v);

  /// The phone number for the linked provider.
  external String get phoneNumber;

  /// The phone number for the linked provider.
  external set phoneNumber(String v);

  /// The photo URL for the linked provider.
  external String get photoURL;

  /// The photo URL for the linked provider.
  external set photoURL(String v);

  /// The linked provider ID (for example, "google.com" for the
  /// Google provider).
  external String get providerId;

  /// The linked provider ID (for example, "google.com" for the
  /// Google provider).
  external set providerId(String v);

  /// The user identifier for the linked provider.
  external String get uid;

  /// The user identifier for the linked provider.
  external set uid(String v);
}

/// Interface representing a user.
@JS('UserRecord')
@anonymous
abstract class UserRecordJsImpl {
  /// Interface representing a user.
  ///
  /// Optional `customClaims`
  /// The user's custom claims object if available, typically used to define
  /// user roles and propagated to an authenticated user's ID token. This is
  /// set via setCustomUserClaims()
  ///
  /// `disabled`
  /// Whether or not the user is disabled: true for disabled; false for enabled.
  ///
  /// Optional `displayName`
  /// The user's display name.
  ///
  /// Optional `email`
  /// The user's primary email, if set.
  ///
  /// `emailVerified`
  /// Whether or not the user's primary email is verified.
  ///
  /// `metadata`
  /// Additional metadata about the user.
  ///
  /// Optional `multiFactor`
  /// The multi-factor related properties for the current user, if available.
  ///
  /// Optional `passwordHash`
  /// The user's hashed password (base64-encoded), only if Firebase Auth
  /// hashing algorithm (SCRYPT) is used. If a different hashing algorithm had b
  /// een used when uploading this user, as is typical when migrating from
  /// another Auth system, this will be an empty string. If no password is set,
  /// this is null. This is only available when the user is obtained from
  /// listUsers().
  ///
  /// Optional `passwordSalt`
  /// The user's password salt (base64-encoded), only if Firebase Auth hashing
  /// algorithm (SCRYPT) is used. If a different hashing algorithm had been
  /// used to upload this user, typical when migrating from another Auth
  /// system, this will be an empty string. If no password is set, this is
  /// null. This is only available when the user is obtained from listUsers().
  ///
  /// Optional `phoneNumber`
  /// The user's primary phone number, if set.
  ///
  /// Optional `photoURL`
  /// The user's photo URL.
  ///
  /// `providerData`
  /// An array of providers (for example, Google, Facebook) linked to the user.
  ///
  /// Optional `tenantId`
  /// The ID of the tenant the user belongs to, if available.
  ///
  /// Optional `tokensValidAfterTime`
  /// The date the user's tokens are valid after, formatted as a UTC string.
  /// This is updated every time the user's refresh token are revoked either
  /// from the revokeRefreshTokens() API or from the Firebase Auth backend on
  /// big account changes (password resets, password or email updates, etc).
  ///
  /// `uid`
  /// The user's uid.
  external factory UserRecordJsImpl({
    dynamic customClaims,
    @required bool disabled,
    String displayName,
    String email,
    @required bool emailVerified,
    @required UserMetadata metadata,
    MultiFactorCreateSettings multiFactor,
    String passwordHash,
    String passwordSalt,
    String phoneNumber,
    String photoURL,
    @required List<UserInfo> providerData,
    String tenantId,
    String tokensValidAfterTime,
    @required String uid,
  });

  external dynamic get customClaims;
  external set customClaims(dynamic v);

  external bool get disabled;
  external set disabled(bool v);

  external String get displayName;
  external set displayName(String v);

  external String get email;
  external set email(String v);

  external bool get emailVerified;
  external set emailVerified(bool v);

  external UserMetadata get metadata;
  external set metadata(UserMetadata v);

  external MultiFactorCreateSettings get multiFactor;
  external set multiFactor(MultiFactorCreateSettings v);

  external String get passwordHash;
  external set passwordHash(String v);

  external String get passwordSalt;
  external set passwordSalt(String v);

  external String get phoneNumber;
  external set phoneNumber(String v);

  external String get photoURL;
  external set photoURL(String v);

  external List<UserInfo> get providerData;
  external set providerData(List<UserInfo> v);

  external String get tenantId;
  external set tenantId(String v);

  external String get tokensValidAfterTime;
  external set tokensValidAfterTime(String v);

  external String get uid;
  external set uid(String v);
}
