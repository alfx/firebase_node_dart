// ignore_for_file: public_member_api_docs
@JS('firebaseAdmin.firestore')
library firebase_node.interop.firestore;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';
import '../../utils/func.dart';
import 'admin_interop.dart';

/// Used internally to allow calling FieldValue.arrayUnion and arrayRemove
@JS('firebaseAdmin.firestore.FieldValue')
external dynamic get fieldValues;

// final FirestoreModuleJsImpl firestoreModule = require('firebase-admin');

// @JS()
// @anonymous
// abstract class FirestoreModuleJsImpl {
//   // ignore: non_constant_identifier_names
//   external FieldPathJsImpl FieldPath(
//     String fieldName0, [
//     String fieldName1,
//     String fieldName2,
//     String fieldName3,
//     String fieldName4,
//     String fieldName5,
//     String fieldName6,
//     String fieldName7,
//     String fieldName8,
//     String fieldName9,
//   ]);
//   // ignore: non_constant_identifier_names
//   external GeoPointJsImpl GeoPoint(num latitude, num longitude);
//   // external TimestampJsImpl Timestamp(int seconds, int nanoseconds);
// }

/// Sets the verbosity of Cloud Firestore logs.
///
/// Parameter [logLevel] is the verbosity you set for activity and error
/// logging.
///
/// Can be any of the following values:
/// * 'debug' for the most verbose logging level, primarily for debugging.
/// * 'error' to log errors only.
/// * 'silent' to turn off logging.
@JS()
external void setLogLevel(String logLevel);

@JS('CollectionReference')
class CollectionReferenceJsImpl extends QueryJsImpl {
  external String get id;
  external DocumentReferenceJsImpl get parent;
  external String get path;
  external PromiseJsImpl<DocumentReferenceJsImpl> add(dynamic data);
  external DocumentReferenceJsImpl doc([String documentPath]);
}

@anonymous
@JS()
abstract class DocumentChangeJsImpl {
  external DocumentSnapshotJsImpl get doc;
  external num get newIndex;
  external num get oldIndex;
  external String /*'added'|'removed'|'modified'*/ get type;
}

/// Options for use with [DocumentReference.onMetadataChangesSnapshot()] to
/// control the behavior of the snapshot listener.
@JS()
@anonymous
abstract class DocumentListenOptions {
  external factory DocumentListenOptions({bool includeMetadataChanges});

  /// Raise an event even if only metadata of the document changed. Default is
  /// [:false:].
  external bool get includeMetadataChanges;

  external set includeMetadataChanges(bool v);
}

@JS('DocumentReference')
abstract class DocumentReferenceJsImpl {
  external FirestoreJsImpl get firestore;
  external String get id;
  external CollectionReferenceJsImpl get parent;
  external String get path;
  external CollectionReferenceJsImpl collection(String collectionPath);
  external PromiseJsImpl<WriteResultJsImpl> create(dynamic data);
  external PromiseJsImpl<WriteResultJsImpl> delete();
  external PromiseJsImpl<DocumentSnapshotJsImpl> get();
  external PromiseJsImpl<List> listCollections();
  external void Function() onSnapshot(
    dynamic Function(DocumentSnapshotJsImpl) onNext, [
    dynamic Function(FirebaseErrorJsImpl) onError,
  ]);
  external PromiseJsImpl<WriteResultJsImpl> set(dynamic data,
      [SetOptions options]);
  external PromiseJsImpl<WriteResultJsImpl> update(
      dynamic dataOrFieldsAndValues);
}

@JS('DocumentSnapshot')
abstract class DocumentSnapshotJsImpl {
  external TimestampJsImpl get createTime;
  external bool get exists;
  external String get id;
  external TimestampJsImpl get readTime;
  external DocumentReferenceJsImpl get ref;
  external TimestampJsImpl get updateTime;
  external dynamic data();
  external dynamic get(dynamic /*String|FieldPath*/ fieldPath);

  /// Returns [true] if this [DocumentSnapshotJsImpl] is equal to the provided
  /// one.
  external bool isEqual(DocumentSnapshotJsImpl other);
}

/// A [FieldPath] refers to a field in a document.
/// The path may consist of a single field name (referring to a top-level
/// field in the document), or a list of field names (referring to a nested
/// field in the document).
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.FieldPath>.
@JS('FieldPath')
// @anonymous
class FieldPath {
  /// Creates a [FieldPath] from the provided field names. If more than one
  /// field name is provided, the path will point to a nested field in
  /// a document.
  external factory FieldPath(String fieldName0,
      [String fieldName1,
      String fieldName2,
      String fieldName3,
      String fieldName4,
      String fieldName5,
      String fieldName6,
      String fieldName7,
      String fieldName8,
      String fieldName9]);

  /// Returns `true` if this [FieldPath] is equal to the [other].
  external bool isEqual(Object other);

  /// Returns a special sentinel FieldPath to refer to the ID of a document.
  /// It can be used in queries to sort or filter by the document ID.
  external static FieldPath documentId();
}

/// Sentinel values that can be used when writing document fields with
/// [set()] or [update()].
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.FieldValue>.
@JS('FieldValue')
abstract class FieldValue {
  /// Returns `true` if this [FieldValue] is equal to the provided [other].
  external bool isEqual(Object other);

  /// Returns a sentinel for use with [update()] to mark a field for deletion.
  external static FieldValue delete();

  external static FieldValue increment(num n);

  /// Returns a sentinel used with [set()] or [update()] to include a
  /// server-generated timestamp in the written data.
  external static FieldValue serverTimestamp();
}

@JS('Firestore')
abstract class FirestoreJsImpl {
  external WriteBatchJsImpl batch();
  external CollectionReferenceJsImpl collection(String collectionPath);
  external QueryJsImpl collectionGroup(String collectionId);
  external DocumentReferenceJsImpl doc(String documentPath);
  external PromiseJsImpl<List /*DocumentSnapshot*/ > getAll(dynamic params);
  external PromiseJsImpl<List> listCollections();
  external PromiseJsImpl runTransaction(
      PromiseJsImpl Function(TransactionJsImpl) updateFunction);
  external void settings(FirestoreSettings settings);
  external PromiseJsImpl<void> terminate();
}

/// Specifies custom configurations for your Cloud Firestore instance.
/// You must set these before invoking any other methods.
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.Settings>.
@JS()
@anonymous
abstract class FirestoreSettings {
  external factory FirestoreSettings({
    int cacheSizeBytes,
    String host,
    bool ssl,
  });

  external int get cacheSizeBytes;
  external set cacheSizeBytes(int i);

  external String get host;
  external set host(String h);

  external bool get ssl;
  external set ssl(bool v);
}

/// An immutable object representing a geo point in Cloud
/// The geo point is represented as latitude/longitude pair.
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.GeoPoint>.
@JS('GeoPoint')
// @anonymous
class GeoPoint {
  /// Creates a new immutable [GeoPoint] object with the provided [latitude] and
  /// [longitude] values.
  ///
  /// [latitude] values are in the range of -90 to 90.
  /// [longitude] values are in the range of -180 to 180.
  external factory GeoPoint(num latitude, num longitude);

  /// The latitude of this GeoPoint instance.
  external num get latitude;

  /// The longitude of this GeoPoint instance.
  external num get longitude;

  /// Returns `true` if this [GeoPoint] is equal to the provided [other].
  external bool isEqual(Object other);
}

@JS()
@anonymous
abstract class PreconditionJsImpl {
  external factory PreconditionJsImpl({TimestampJsImpl lastUpdateTime});
  external TimestampJsImpl get lastUpdateTime;
  external set lastUpdateTime(TimestampJsImpl v);
}

@JS('Query')
abstract class QueryJsImpl {
  external FirestoreJsImpl get firestore;

  external QueryJsImpl endAt(
      dynamic /*DocumentSnapshot|List<dynamic>*/ snapshotOrFieldValues);

  external QueryJsImpl endBefore(
      dynamic /*DocumentSnapshot|List<dynamic>*/ snapshotOrFieldValues);

  external PromiseJsImpl<QuerySnapshotJsImpl> get();

  external bool isEqual(QueryJsImpl other);

  external QueryJsImpl limit(num limit);

  external QueryJsImpl limitToLast(num limit);

  external QueryJsImpl offset(num offset);

  external void Function() onSnapshot(
      // SnapshotListenOptions options,
      void Function(QuerySnapshotJsImpl) onNext,
      Func1<FirebaseErrorJsImpl, dynamic> onError);

  external QueryJsImpl orderBy(
    dynamic /*String|FieldPath*/ fieldPath, [
    String /*'desc'|'asc'*/ directionStr,
  ]);

  external QueryJsImpl select(dynamic /*String|FieldPath*/ fieldPath);

  external QueryJsImpl startAfter(
      dynamic /*DocumentSnapshot|List<dynamic>*/ snapshotOrFieldValues);

  external QueryJsImpl startAt(
      dynamic /*DocumentSnapshot|List<dynamic>*/ snapshotOrFieldValues);

  external QueryJsImpl where(dynamic /*String|FieldPath*/ fieldPath,
      String /*'<'|'<='|'=='|'>='|'>'*/ opStr, dynamic value);
}

@JS('QuerySnapshot')
abstract class QuerySnapshotJsImpl {
  external List<DocumentSnapshotJsImpl> get docs;

  external bool get empty;

  external QueryJsImpl get query;

  external TimestampJsImpl get readTime;

  external num get size;

  external List<DocumentChangeJsImpl> docChanges();

  external void forEach(
    void Function(DocumentSnapshotJsImpl) callback, [
    dynamic thisArg,
  ]);

  external bool isEqual(QuerySnapshotJsImpl other);
}

/// An object to configure the [WriteBatch.set] behavior.
/// Pass [: {merge: true} :] to only replace the values specified in
/// the data argument. Fields omitted will remain untouched.
@JS()
@anonymous
abstract class SetOptions {
  external factory SetOptions({bool merge});

  /// Set to true to replace only the values from the new data.
  /// Fields omitted will remain untouched.
  external bool get merge;

  external set merge(bool v);
}

@JS('Timestamp')
abstract class TimestampJsImpl {
  external factory TimestampJsImpl(int seconds, int nanoseconds);

  external num get nanoseconds;

  external num get seconds;

  //external JsDate toDate();
  external bool isEqual(TimestampJsImpl other);

  external int toMillis();

  //external static TimestampJsImpl fromDate(JsDate date);
  @override
  external String toString();

  external String valueOf();

  external static TimestampJsImpl fromMillis(int milliseconds);

  external static TimestampJsImpl now();
}

@JS('Transaction')
abstract class TransactionJsImpl {
  external PromiseJsImpl<List /*DocumentSnapshot*/ > getAll(dynamic params);
  external TransactionJsImpl create(DocumentReferenceJsImpl ref, dynamic data);
  external TransactionJsImpl delete(DocumentReferenceJsImpl documentRef);
  external PromiseJsImpl get(dynamic refOrQuery);
  external TransactionJsImpl set(
      DocumentReferenceJsImpl documentRef, dynamic data,
      [SetOptions options]);
  external TransactionJsImpl update(
      DocumentReferenceJsImpl documentRef, dynamic dataOrFieldsAndValues);
}

@JS('WriteBatch')
abstract class WriteBatchJsImpl {
  external PromiseJsImpl<WriteResultJsImpl> commit();

  external WriteBatchJsImpl create(DocumentReferenceJsImpl ref, dynamic data);

  external WriteBatchJsImpl delete(DocumentReferenceJsImpl documentRef);

  external WriteBatchJsImpl set(
      DocumentReferenceJsImpl documentRef, dynamic data,
      [SetOptions options]);

  external WriteBatchJsImpl update(
      DocumentReferenceJsImpl documentRef, dynamic dataOrFieldsAndValues);
}

@JS('WriteResult')
abstract class WriteResultJsImpl {
  external TimestampJsImpl get writeTime;
  external bool isEqual(WriteResultJsImpl other);
}
