// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.interop.remote_config;

import 'package:js/js.dart';
import 'package:meta/meta.dart';

import '../../utils/es6_interop.dart';
import 'app_interop.dart';

@JS()
@anonymous
abstract class ExplicitParameterValue {
  external factory ExplicitParameterValue({
    String value,
  });
  external String get value;
  external set value(String v);
}

@JS()
@anonymous
abstract class InAppDefaultValue {
  external factory InAppDefaultValue({
    bool useInAppDefault,
  });
  external bool get useInAppDefault;
  external set useInAppDefault(bool v);
}

@JS()
@anonymous
abstract class PublishOptions {
  external factory PublishOptions({bool force});
  external bool get force;
  external set force(bool v);
}

@JS()
@anonymous
abstract class RemoteConfigCondition {
  external factory RemoteConfigCondition({
    @required String value,
    @required String expression,
    String tagColor,
  });
  external String get expression;
  external set expression(String v);
  external String get name;
  external set name(String v);
  external String get tagColor;
  external set tagColor(String v);
}

@JS('RemoteConfig')
@anonymous
abstract class RemoteConfigJsImpl {
  external AppJsImpl get app;
  external RemoteConfigTemplateJsImpl createTemplateFromJSON(String json);
  external PromiseJsImpl<RemoteConfigTemplateJsImpl> getTemplate();
  external PromiseJsImpl<RemoteConfigTemplateJsImpl> publishTemplate(
      RemoteConfigTemplateJsImpl template,
      [PublishOptions options]);
  external PromiseJsImpl<RemoteConfigTemplateJsImpl> validateTemplate(
      RemoteConfigTemplateJsImpl template);
}

@JS('RemoteConfigParameterGroup')
@anonymous
abstract class RemoteConfigParameterGroupJsImpl {
  external factory RemoteConfigParameterGroupJsImpl({
    String description,
    dynamic parameters,
  });

  external String get description;
  external set description(String v);

  external dynamic /*Map<String,RemoteConfigParameter> */ get parameters;
  external set parameters(dynamic /*Map<String,RemoteConfigParameter> */ v);
}

@JS('RemoteConfigParameter')
@anonymous
abstract class RemoteConfigParameterJsImpl {
  external factory RemoteConfigParameterJsImpl({
    dynamic conditionalValues,
    ExplicitParameterValue defaultValue,
    String description,
  });
  external dynamic /*Map<String,dynamic> */ get conditionalValues;
  external set conditionalValues(dynamic /*Map<String,dynamic> */ v);
  external ExplicitParameterValue get defaultValue;
  external set defaultValue(ExplicitParameterValue v);
  external String get description;
  external set description(String v);
}

@JS('RemoteConfigTemplate')
@anonymous
abstract class RemoteConfigTemplateJsImpl {
  external List /*RemoteConfigCondition */ get conditions;
  external set conditions(List /*RemoteConfigCondition */ v);

  external String get etag;
  external set etag(String v);

  external dynamic /*Map<String,RemoteConfigParameterGroup>*/
      get parameterGroups;
  external set parameterGroups(
      dynamic /*Map<String,RemoteConfigParameterGroup>*/ v);

  external dynamic /*Map<String,RemoteConfigParameter>*/ get parameters;
  external set parameters(dynamic /*Map<String,RemoteConfigParameter>*/ v);
}
