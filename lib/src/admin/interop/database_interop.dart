// ignore_for_file: public_member_api_docs
@JS('firebaseAdmin.database')
library firebase_node.interop.database;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';
import '../../utils/func.dart';
import 'app_interop.dart';

// ignore: avoid_positional_boolean_parameters
external void enableLogging([dynamic logger, bool persistent]);

@JS('Database')
@anonymous
abstract class DatabaseJsImpl {
  external AppJsImpl get app;
  external set app(AppJsImpl a);
  external void goOffline();
  external void goOnline();
  external ReferenceJsImpl ref([String path]);
  external ReferenceJsImpl refFromURL(String url);
  external PromiseJsImpl<String> getRules();
  external PromiseJsImpl getRulesJSON();
  external PromiseJsImpl<void> setRules(dynamic source);
}

@JS('DataSnapshot')
@anonymous
abstract class DataSnapshotJsImpl {
  external String get key;
  external set key(String s);
  external ReferenceJsImpl get ref;
  external set ref(ReferenceJsImpl r);
  external DataSnapshotJsImpl child(String path);
  external bool exists();
  external dynamic exportVal();
  external bool forEach(Func1 action);
  external dynamic getPriority();
  external bool hasChild(String path);
  external bool hasChildren();
  external int numChildren();
  external Object toJSON();
  external dynamic val();
}

@JS('OnDisconnect')
@anonymous
abstract class OnDisconnectJsImpl {
  external PromiseJsImpl<void> cancel([Func1 onComplete]);
  external PromiseJsImpl<void> remove([Func1 onComplete]);
  external PromiseJsImpl<void> set(dynamic value, [Func1 onComplete]);
  external PromiseJsImpl<void> setWithPriority(dynamic value, dynamic priority,
      [Func1 onComplete]);
  external PromiseJsImpl<void> update(dynamic values, [Func1 onComplete]);
}

@JS('Query')
@anonymous
abstract class QueryJsImpl {
  external ReferenceJsImpl get ref;
  external set ref(ReferenceJsImpl r);
  external QueryJsImpl endAt(dynamic value, [String key]);
  external QueryJsImpl equalTo(dynamic value, [String key]);
  external bool isEqual(QueryJsImpl other);
  external QueryJsImpl limitToFirst(int limit);
  external QueryJsImpl limitToLast(int limit);
  external void off(
      [String eventType,
      void Function(DataSnapshotJsImpl, [String]) callback,
      dynamic context]);
  external dynamic Function() on(
      String eventType, void Function(DataSnapshotJsImpl, [String]) callback,
      [dynamic cancelCallbackOrContext, dynamic context]);
  external PromiseJsImpl<dynamic> once(String eventType,
      [void Function(DataSnapshotJsImpl, [String]) successCallback,
      dynamic failureCallbackOrContext,
      dynamic context]);
  external QueryJsImpl orderByChild(String path);
  external QueryJsImpl orderByKey();
  external QueryJsImpl orderByPriority();
  external QueryJsImpl orderByValue();
  external QueryJsImpl startAt(dynamic value, [String key]);
  external Object toJSON();
  @override
  external String toString();
}

@JS('Reference')
@anonymous
abstract class ReferenceJsImpl extends QueryJsImpl {
  external String get path;
  external set path(String v);
  external String get key;
  external set key(String s);
  external ReferenceJsImpl get parent;
  external set parent(ReferenceJsImpl r);
  external ReferenceJsImpl get root;
  external set root(ReferenceJsImpl r);
  external ReferenceJsImpl child(String path);
  external OnDisconnectJsImpl onDisconnect();
  external ThenableReferenceJsImpl push([dynamic value, Func1 onComplete]);
  external PromiseJsImpl<void> remove([Func1 onComplete]);
  external PromiseJsImpl<void> set(dynamic value, [Func1 onComplete]);
  external PromiseJsImpl<void> setPriority(dynamic priority,
      [Func1 onComplete]);
  external PromiseJsImpl<void> setWithPriority(
      dynamic newVal, dynamic newPriority,
      [Func1 onComplete]);
  external PromiseJsImpl<TransactionJsImpl> transaction(Func1 transactionUpdate,
      [void Function(Object, bool, DataSnapshotJsImpl) onComplete,
      // ignore: avoid_positional_boolean_parameters
      bool applyLocally]);
  external PromiseJsImpl<void> update(dynamic values, [Func1 onComplete]);
}

// ignore: avoid_classes_with_only_static_members
/// A placeholder value for auto-populating the current timestamp
/// (time since the Unix epoch, in milliseconds) as determined
/// by the Firebase servers.
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.database#.ServerValue>.
@JS()
abstract class ServerValue {
// ignore: non_constant_identifier_names
  external static Object get TIMESTAMP;
}

@JS('ThenableReference')
@anonymous
abstract class ThenableReferenceJsImpl extends ReferenceJsImpl
    implements PromiseJsImpl<ReferenceJsImpl> {
  @override
  external PromiseJsImpl<void> then([Func1 onResolve, Func1 onReject]);
}

@JS()
@anonymous
class TransactionJsImpl {
  external factory TransactionJsImpl(
      {bool committed, DataSnapshotJsImpl snapshot});
  external bool get committed;

  external DataSnapshotJsImpl get snapshot;
}
