part of '../../admin.dart';

/// Gets the InstanceId service for the current app.
class InstanceId extends JsObjectWrapper<instance_id_interop.InstanceIdJsImpl> {
  static final _expando = Expando<InstanceId>();

  InstanceId._fromJsObject(instance_id_interop.InstanceIdJsImpl jsObject)
      : super.fromJsObject(jsObject);

  /// Deletes the specified instance ID and the associated data from Firebase.
  ///
  /// Note that Google Analytics for Firebase uses its own form of Instance ID
  /// to keep track of analytics data. Therefore deleting a Firebase Instance
  /// ID does not delete Analytics data. See [Delete an Instance ID](https://firebase.google.com/support/privacy/manage-iids#delete_an_instance_id) for more
  /// information.
  ///
  /// Parameters
  /// - `instanceId`:
  /// The instance ID to be deleted.
  ///
  /// Returns Future<void>
  /// A promise fulfilled when the instance ID is deleted.
  Future<void> deleteInstanceId(String instanceId) =>
      handleThenable(jsObject.deleteInstanceId(instanceId));

  /// Gets the InstanceId service for the current app.
  static InstanceId getInstance(instance_id_interop.InstanceIdJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= InstanceId._fromJsObject(jsObject);
  }
}
