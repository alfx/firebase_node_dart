part of '../../admin.dart';

Map<String, dynamic> _listToMap(List list) => dartify(list.first);

Future<StorageResponse<T>> _makeResponse<T, S>(
  Future<List> future,
  T Function(S o) mapper,
) async {
  final result = await future;
  final data = mapper(result[0]);
  final map = dartify(result[1]);
  return StorageResponse(data, map);
}

class Bucket extends JsObjectWrapper<storage_interop.BucketJsImpl>
    implements _Bucket {
  static final _expando = Expando<Bucket>();

  Bucket._fromJsObject(storage_interop.BucketJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Future<Map<String, dynamic>> addLifecycleRule(LifeCycleRule rule,
      [storage_interop.AddLifecycleRuleOptions options]) {
    Future<List> future;
    if (options == null) {
      future = handleThenable(jsObject.addLifecycleRule(rule.jsObject));
    } else {
      future =
          handleThenable(jsObject.addLifecycleRule(rule.jsObject, options));
    }
    return future.then(_listToMap);
  }

  @override
  Future<StorageResponse<File>> combine(
    List /*String|File*/ sources,
    dynamic /*String,File*/ destination, [
    storage_interop.CombineOptions options,
  ]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(
        jsObject.combine(
          jsify(sources),
          jsify(destination),
          options,
        ),
      );
    } else {
      future = handleThenable(
        jsObject.combine(
          jsify(sources),
          jsify(destination),
        ),
      );
    }
    return _makeResponse(future, File.getInstance);
  }

  @override
  Future<StorageResponse<Bucket>> create([CreateBucketRequest metadata]) {
    Future<List> future;
    if (metadata != null) {
      future = handleThenable(jsObject.create(metadata.jsObject));
    } else {
      future = handleThenable(jsObject.create());
    }
    return _makeResponse(future, Bucket.getInstance);
  }

  @override
  Future<StorageResponse<Channel>> createChannel(
    String id,
    storage_interop.CreateChannelConfig config, [
    storage_interop.StorageBasicOptions options,
  ]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.createChannel(id, config, options));
    } else {
      future = handleThenable(jsObject.createChannel(id, config));
    }
    return _makeResponse(future, Channel.getInstance);
  }

  @override
  Future<StorageResponse<StorageNotification>> createNotification(
    String topic, [
    CreateNotificationOptions options,
  ]) {
    Future<List> future;
    if (options != null) {
      future =
          handleThenable(jsObject.createNotification(topic, options.jsObject));
    } else {
      future = handleThenable(jsObject.createNotification(topic));
    }
    return _makeResponse(future, StorageNotification.getInstance);
  }

  @override
  Future<Map<String, dynamic>> delete(
      storage_interop.StorageBasicOptions options) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.delete(options));
    } else {
      future = handleThenable(jsObject.delete());
    }
    return future.then(_listToMap);
  }

  @override
  Future<void> deleteFiles([storage_interop.DeleteFilesOptions options]) {
    if (options != null) {
      return handleThenable(jsObject.deleteFiles(options));
    } else {
      return handleThenable(jsObject.deleteFiles());
    }
  }

  @override
  Future<Map<String, dynamic>> deleteLabels(List<String> labels) =>
      handleThenable(jsObject.deleteLabels(labels)).then(_listToMap);

  @override
  Future<Map<String, dynamic>> disableRequesterPays() =>
      handleThenable(jsObject.disableRequesterPays())
          .then((value) => dartify(value?.first));

  @override
  Future<Map<String, dynamic>> enableLogging(EnableLoggingOptions options) =>
      handleThenable(jsObject.enableLogging(options.jsObject))
          .then((value) => dartify(value?.first));

  @override
  Future<Map<String, dynamic>> enableRequesterPays() =>
      handleThenable(jsObject.enableRequesterPays())
          .then((value) => dartify(value?.first));

  @override
  Future<bool> exists([storage_interop.BucketExistsOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.exists(options));
    } else {
      future = handleThenable(jsObject.exists());
    }
    return future.then((value) => dartify(value.first));
  }

  @override
  File file(String name, [storage_interop.FileOptions options]) {
    if (options != null) {
      return File.getInstance(jsObject.file(name, options));
    } else {
      return File.getInstance(jsObject.file(name));
    }
  }

  @override
  Future<StorageResponse<Bucket>> get(
      [storage_interop.GetBucketOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.get(options));
    } else {
      future = handleThenable(jsObject.get());
    }
    return _makeResponse(future, Bucket.getInstance);
  }

  @override
  Future<List<File>> getFiles([storage_interop.GetFilesOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.getFiles(options));
    } else {
      future = handleThenable(jsObject.getFiles());
    }
    return future.then((value) =>
        // ignore: unnecessary_lambdas
        (value.first as List).map<File>((e) => File.getInstance(e)).toList());
  }

  @override
  Future<Map<String, dynamic>> getLabels(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.getLabels(options));
    } else {
      future = handleThenable(jsObject.getLabels());
    }
    return future.then(_listToMap);
  }

  @override
  Future<StorageResponse<Map<String, dynamic>>> getMetadata(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.getMetadata(options));
    } else {
      future = handleThenable(jsObject.getMetadata());
    }
    return _makeResponse(future, (o) => dartify(o) as Map<String, dynamic>);
  }

  @override
  Future<StorageResponse<List<StorageNotification>>> getNotifications(
      [storage_interop.StorageBasicOptions options]) async {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.getNotifications(options));
    } else {
      future = handleThenable(jsObject.getNotifications());
    }
    final result = await future;
    final data = (result[0] as List)
        // ignore: unnecessary_lambdas
        .map((e) => StorageNotification.getInstance(e))
        .toList();
    final object = dartify(result[1]);
    return StorageResponse(data, object);
  }

  @override
  Future<String> getSignedUrl(GetBucketSignedUrlConfig config) =>
      handleThenable(jsObject.getSignedUrl(config.jsObject))
          .then((value) => value.first as String);

  @override
  Future<Map<String, dynamic>> lock(dynamic /*num | String*/ metageneration) {
    assert(metageneration is num || metageneration is String);
    return handleThenable(jsObject.lock(metageneration)).then(_listToMap);
  }

  @override
  Future<List<File>> makePrivate(
      [storage_interop.MakeBucketPrivateOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.makePrivate(options));
    } else {
      future = handleThenable(jsObject.makePrivate());
    }
    return future.then((value) =>
        // ignore: unnecessary_lambdas
        (value.first as List).map((e) => File.getInstance(e)).toList());
  }

  @override
  Future<List<File>> makePublic(
      [storage_interop.MakeBucketPublicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.makePublic(options));
    } else {
      future = handleThenable(jsObject.makePublic());
    }
    return future.then((value) =>
        // ignore: unnecessary_lambdas
        (value.first as List).map((e) => File.getInstance(e)).toList());
  }

  @override
  StorageNotification notification(String id) =>
      StorageNotification.getInstance(jsObject.notification(id));

  @override
  Future<Map<String, dynamic>> removeRetentionPeriod() =>
      handleThenable(jsObject.removeRetentionPeriod()).then(_listToMap);

  @override
  Future<Map<String, dynamic>> setLabels(Map<String, String> labels,
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.setLabels(jsify(labels), options));
    } else {
      future = handleThenable(jsObject.setLabels(jsify(labels)));
    }
    return future.then(_listToMap);
  }

  @override
  Future<Map<String, dynamic>> setMetadata(Map<String, String> metadata,
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.setMetadata(jsify(metadata), options));
    } else {
      future = handleThenable(jsObject.setMetadata(jsify(metadata)));
    }
    return future.then(_listToMap);
  }

  @override
  Future<Map<String, dynamic>> setRetentionPeriod(int duration) =>
      handleThenable(jsObject.setRetentionPeriod(duration)).then(_listToMap);

  @override
  Future<Map<String, dynamic>> setStorageClass(String storageClass,
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.setStorageClass(storageClass, options));
    } else {
      future = handleThenable(jsObject.setStorageClass(storageClass));
    }
    return future.then(_listToMap);
  }

  @override
  void setUserProject(String userProject) =>
      jsObject.setUserProject(userProject);

  @override
  Future<StorageResponse<File>> upload(String path, [UploadOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.upload(path, options.jsObject));
    } else {
      future = handleThenable(jsObject.upload(path));
    }
    return _makeResponse(future, File.getInstance);
  }

  static Bucket getInstance(storage_interop.BucketJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Bucket._fromJsObject(jsObject);
  }
}

class Channel extends JsObjectWrapper<storage_interop.ChannelJsImpl> {
  static final _expando = Expando<Channel>();

  Channel._fromJsObject(storage_interop.ChannelJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Future<void> stop() => handleThenable(jsObject.stop());

  static Channel getInstance(storage_interop.ChannelJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Channel._fromJsObject(jsObject);
  }
}

class Cors extends JsObjectWrapper<storage_interop.CorsJsImpl> {
  static final _expando = Expando<Cors>();

  factory Cors({
    num maxAgeSeconds,
    List<String> method,
    List<String> origin,
    List<String> responseHeader,
  }) =>
      Cors.getInstance(storage_interop.CorsJsImpl(
        maxAgeSeconds: maxAgeSeconds,
        method: method,
        origin: origin,
        responseHeader: responseHeader,
      ));

  Cors._fromJsObject(storage_interop.CorsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  num get maxAgeSeconds => jsObject.maxAgeSeconds;

  set maxAgeSeconds(num v) => jsObject.maxAgeSeconds = v;
  List<String> get method => List.from(jsObject.method);

  set method(List<String> v) => jsObject.method = v;
  List<String> get origin => List.from(jsObject.origin);

  set origin(List<String> v) => jsObject.origin = v;
  List<String> get responseHeader => List.from(jsObject.responseHeader);

  set responseHeader(List<String> v) => jsObject.responseHeader = v;
  static Cors getInstance(storage_interop.CorsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Cors._fromJsObject(jsObject);
  }
}

class CreateBucketRequest
    extends JsObjectWrapper<storage_interop.CreateBucketRequestJsImpl> {
  static final _expando = Expando<CreateBucketRequest>();

  factory CreateBucketRequest({
    bool archive,
    bool coldline,
    List<Cors> cors,
    bool dra,
    bool multiRegional,
    bool nearline,
    bool regional,
    bool requesterPays,
    bool standard,
    bool userProject,
    storage_interop.Versioning versioning,
  }) =>
      CreateBucketRequest._fromJsObject(
          storage_interop.CreateBucketRequestJsImpl(
        archive: archive,
        coldline: coldline,
        cors: jsify(cors),
        dra: dra,
        multiRegional: multiRegional,
        nearline: nearline,
        requesterPays: requesterPays,
        standard: standard,
        userProject: userProject,
        versioning: versioning,
      ));

  CreateBucketRequest._fromJsObject(
      storage_interop.CreateBucketRequestJsImpl jsObject)
      : super.fromJsObject(jsObject);

  bool get archive => jsObject.archive;

  set archive(bool v) => jsObject.archive = v;
  bool get coldline => jsObject.coldline;

  set coldline(bool v) => jsObject.coldline = v;
  // ignore: unnecessary_lambdas
  List<Cors> get cors => jsObject.cors.map((e) => Cors.getInstance(e)).toList();

  // ignore: unnecessary_lambdas
  set cors(List<Cors> v) => jsObject.cors = v;
  bool get dra => jsObject.dra;

  set dra(bool v) => jsObject.dra = v;
  bool get multiRegional => jsObject.multiRegional;

  set multiRegional(bool v) => jsObject.multiRegional = v;
  bool get nearline => jsObject.nearline;

  set nearline(bool v) => jsObject.nearline = v;
  bool get regional => jsObject.regional;

  set regional(bool v) => jsObject.regional = v;
  bool get requesterPays => jsObject.requesterPays;

  set requesterPays(bool v) => jsObject.requesterPays = v;
  bool get standard => jsObject.standard;

  set standard(bool v) => jsObject.standard = v;
  bool get userProject => jsObject.userProject;

  set userProject(bool v) => jsObject.userProject = v;
  storage_interop.Versioning get versioning => jsObject.versioning;

  set versioning(storage_interop.Versioning v) => jsObject.versioning = v;
  static CreateBucketRequest getInstance(
      storage_interop.CreateBucketRequestJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= CreateBucketRequest._fromJsObject(jsObject);
  }
}

class CreateNotificationOptions
    extends JsObjectWrapper<storage_interop.CreateNotificationOptionsJsImpl> {
  static final _expando = Expando<CreateNotificationOptions>();

  factory CreateNotificationOptions({
    Map<String, String> customAttributes,
    String eventTypes,
    String objectNamePrefix,
    String payloadFormat,
    String userProject,
  }) =>
      CreateNotificationOptions._fromJsObject(
          storage_interop.CreateNotificationOptionsJsImpl(
        customAttributes: jsify(customAttributes),
        eventTypes: eventTypes,
        objectNamePrefix: objectNamePrefix,
        payloadFormat: payloadFormat,
        userProject: userProject,
      ));

  CreateNotificationOptions._fromJsObject(
      storage_interop.CreateNotificationOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, dynamic> get customAttributes {
    final map = dartify(jsObject.customAttributes);
    if (map == null) return null;
    return (map as Map<String, dynamic>).cast<String, String>();
  }

  set customAttributes(Map<String, dynamic> v) =>
      jsObject.customAttributes = jsify(v);

  String get eventTypes => jsObject.eventTypes;

  set eventTypes(String v) => jsObject.eventTypes = v;
  String get objectNamePrefix => jsObject.objectNamePrefix;

  set objectNamePrefix(String v) => jsObject.objectNamePrefix = v;
  String get payloadFormat => jsObject.payloadFormat;

  set payloadFormat(String v) => jsObject.payloadFormat = v;
  String get userProject => jsObject.userProject;

  set userProject(String v) => jsObject.userProject = v;
  static CreateNotificationOptions getInstance(
      storage_interop.CreateNotificationOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        CreateNotificationOptions._fromJsObject(jsObject);
  }
}

class CreateNotificationResponse {
  final StorageNotification notification;
  final Map<String, dynamic> object;
  CreateNotificationResponse(this.notification, this.object);
}

class CreateResumableUploadOptions extends JsObjectWrapper<
    storage_interop.CreateResumableUploadOptionsJsImpl> {
  static final _expando = Expando<CreateResumableUploadOptions>();

  CreateResumableUploadOptions._fromJsObject(
      storage_interop.CreateResumableUploadOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  static CreateResumableUploadOptions getInstance(
      storage_interop.CreateResumableUploadOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        CreateResumableUploadOptions._fromJsObject(jsObject);
  }
}

class CreateWriteStreamOptions
    extends JsObjectWrapper<storage_interop.CreateWriteStreamOptionsJsImpl>
    implements _CreateWriteStreamOptions {
  static final _expando = Expando<CreateWriteStreamOptions>();

  CreateWriteStreamOptions._fromJsObject(
      storage_interop.CreateWriteStreamOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  static CreateWriteStreamOptions getInstance(
      storage_interop.CreateWriteStreamOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        CreateWriteStreamOptions._fromJsObject(jsObject);
  }
}

class EnableLoggingOptions
    extends JsObjectWrapper<storage_interop.EnableLoggingOptionsJsImpl> {
  static final _expando = Expando<EnableLoggingOptions>();

  factory EnableLoggingOptions({
    Map<String, Bucket> bucket,
    String prefix,
  }) =>
      EnableLoggingOptions._fromJsObject(
          storage_interop.EnableLoggingOptionsJsImpl(
        bucket: jsify(bucket),
        prefix: prefix,
      ));

  EnableLoggingOptions._fromJsObject(
      storage_interop.EnableLoggingOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, Bucket> get bucket {
    final map = dartify(jsObject.bucket);
    if (map == null) return null;
    return map.map((k, v) => MapEntry(k as String, Bucket.getInstance(v)));
  }

  set bucket(Map<String, Bucket> v) => jsObject.bucket = jsify(v);

  String get prefix => jsObject.prefix;

  set prefix(String v) => jsObject.prefix = v;
  static EnableLoggingOptions getInstance(
      storage_interop.EnableLoggingOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= EnableLoggingOptions._fromJsObject(jsObject);
  }
}

class File extends JsObjectWrapper<storage_interop.FileJsImpl>
    implements _File {
  static final _expando = Expando<File>();

  File._fromJsObject(storage_interop.FileJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Future<StorageResponse<File>> copy(
    dynamic /*String|Bucket|File*/ destination, [
    storage_interop.CopyOptions options,
  ]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.copy(destination));
    } else {
      future = handleThenable(jsObject.copy(destination, options));
    }
    return _makeResponse(future, File.getInstance);
  }

  @override
  Future<Readable> createReadStream(
      [storage_interop.CreateReadStreamOptions options]) {
    if (options != null) {
      return handleThenable(jsObject.createReadStream(options));
    } else {
      return handleThenable(jsObject.createReadStream());
    }
  }

  @override
  Future<String> createResumableUpload([
    CreateResumableUploadOptions options,
  ]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.createResumableUpload(options.jsObject));
    } else {
      future = handleThenable(jsObject.createResumableUpload());
    }
    return future.then((value) => value.first as String);
  }

  @override
  Future<Writable> createWriteStream([CreateWriteStreamOptions options]) {
    if (options != null) {
      return handleThenable(jsObject.createWriteStream(options.jsObject));
    } else {
      return handleThenable(jsObject.createWriteStream());
    }
  }

  @override
  Future<Map<String, dynamic>> delete(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.delete(options));
    } else {
      future = handleThenable(jsObject.delete());
    }
    return future.then(_listToMap);
  }

  @override
  void deleteResumableCache() => jsObject.deleteResumableCache();

  @override
  Future<Buffer> download([storage_interop.CreateReadStreamOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.download(options));
    } else {
      future = handleThenable(jsObject.download());
    }
    return future.then((value) => value.first as Buffer);
  }

  @override
  Future<bool> exists([storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.exists(options));
    } else {
      future = handleThenable(jsObject.exists());
    }
    return future.then((value) => value.first as bool);
  }

  @override
  Future<storage_interop.PolicyDocument> generateSignedPostPolicyV2(
          GetSignedPolicyOptions options) =>
      handleThenable(jsObject.generateSignedPostPolicyV2(options.jsObject))
          .then((value) => value.first as storage_interop.PolicyDocument);

  @override
  Future<StorageResponse<File>> get(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.get(options));
    } else {
      future = handleThenable(jsObject.get());
    }
    return _makeResponse(future, File.getInstance);
  }

  @override
  Future<StorageResponse<DateTime>> getExpirationDate() {
    final future = handleThenable(jsObject.getExpirationDate());
    return _makeResponse(future, (o) => dartify(o) as DateTime);
  }

  @override
  Future<StorageResponse<Map<String, dynamic>>> getMetadata(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;

    if (options != null) {
      future = handleThenable(jsObject.get(options));
    } else {
      future = handleThenable(jsObject.get());
    }
    return _makeResponse(future, _listToMap);
  }

  @override
  Future<storage_interop.PolicyDocument> getSignedPolicy(
      GetSignedPolicyOptions options) {
    return handleThenable(jsObject.getSignedPolicy(options.jsObject))
        .then((value) => value.first as storage_interop.PolicyDocument);
  }

  @override
  Future<bool> isPublic() =>
      handleThenable(jsObject.isPublic()).then((v) => dartify(v.first));

  @override
  Future<Map<String, dynamic>> makePrivate(
      [storage_interop.MakeFilePrivateOptions options]) {
    Future<List> future;
    if (options == null) {
      future = handleThenable(jsObject.makePrivate());
    } else {
      future = handleThenable(jsObject.makePrivate(options));
    }
    return future.then(_listToMap);
  }

  @override
  Future<Map<String, dynamic>> makePublic() {
    return handleThenable(jsObject.makePublic()).then(_listToMap);
  }

  @override
  Future<StorageResponse<File>> move(
      dynamic /*String|Bucket|File*/ destination) {
    final future = handleThenable(jsObject.move(jsify(destination)));
    return _makeResponse(future, File.getInstance);
  }

  @override
  Future<StorageResponse<File>> rotateEncryptionKey(
      [storage_interop.EncryptionKeyOptions options]) {
    Future<List> future;
    if (options == null) {
      future = handleThenable(jsObject.rotateEncryptionKey());
    } else {
      future = handleThenable(jsObject.rotateEncryptionKey(options));
    }
    return _makeResponse(future, File.getInstance);
  }

  @override
  File setEncryptionKey(String encryptionKey) {
    return File.getInstance(jsObject.setEncryptionKey(encryptionKey));
  }

  @override
  Future<Map<String, dynamic>> setMetadata(
      [Map<String, dynamic> metadata,
      storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (metadata == null) {
      future = handleThenable(jsObject.setMetadata());
    } else if (options == null) {
      future = handleThenable(jsObject.setMetadata(jsify(metadata)));
    } else {
      future = handleThenable(jsObject.setMetadata(jsify(metadata), options));
    }
    return future.then(_listToMap);
  }

  @override
  Future<Map<String, dynamic>> setStorageClass(String storageClass,
      [storage_interop.StorageBasicOptions options]) {
    final thenable = options == null
        ? jsObject.setStorageClass(storageClass)
        : jsObject.setStorageClass(storageClass, options);
    return handleThenable(thenable).then(_listToMap);
  }

  @override
  void setUserProject(String userProject) =>
      jsObject.setUserProject(userProject);

  static File getInstance(storage_interop.FileJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= File._fromJsObject(jsObject);
  }
}

class GetBucketSignedUrlConfig
    extends JsObjectWrapper<storage_interop.GetBucketSignedUrlConfigJsImpl> {
  static final _expando = Expando<GetBucketSignedUrlConfig>();

  factory GetBucketSignedUrlConfig({
    String action,
    String cname,
    dynamic /*String|num|JsDate*/ expires,
    Map<String, dynamic> extensionHeaders,
    Map<String, String> queryParams,
    String version,
    bool virtualHostedStyle,
  }) =>
      GetBucketSignedUrlConfig._fromJsObject(
          storage_interop.GetBucketSignedUrlConfigJsImpl(
        action: action,
        cname: cname,
        expires: jsify(expires),
        extensionHeaders: jsify(extensionHeaders),
        queryParams: jsify(queryParams),
        version: version,
        virtualHostedStyle: virtualHostedStyle,
      ));

  GetBucketSignedUrlConfig._fromJsObject(
      storage_interop.GetBucketSignedUrlConfigJsImpl jsObject)
      : super.fromJsObject(jsObject);

  String get action => jsObject.action;

  set action(String v) => jsObject.action = v;
  String get cname => jsObject.cname;

  set cname(String v) => jsObject.cname = v;
  dynamic get expires => dartify(jsObject.expires);

  set expires(dynamic v) => jsObject.expires = jsify(v);
  Map<String, dynamic> get extensionHeaders =>
      dartify(jsObject.extensionHeaders);

  set extensionHeaders(Map<String, dynamic> v) =>
      jsObject.extensionHeaders = jsify(v);
  Map<String, String> get queryParams {
    final map = dartify(jsObject.queryParams);
    if (map == null) return null;
    return (map as Map<String, dynamic>).cast<String, String>();
  }

  set queryParams(Map<String, String> v) => jsObject.queryParams = jsify(v);

  String get version => jsObject.version;

  set version(String v) => jsObject.version = v;
  bool get virtualHostedStyle => jsObject.virtualHostedStyle;

  set virtualHostedStyle(bool v) => jsObject.virtualHostedStyle = v;
  static GetBucketSignedUrlConfig getInstance(
      storage_interop.GetBucketSignedUrlConfigJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        GetBucketSignedUrlConfig._fromJsObject(jsObject);
  }
}

class GetSignedPolicyOptions
    extends JsObjectWrapper<storage_interop.GetSignedPolicyOptionsJsImpl> {
  static final _expando = Expando<GetSignedPolicyOptions>();

  GetSignedPolicyOptions._fromJsObject(
      storage_interop.GetSignedPolicyOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  static GetSignedPolicyOptions getInstance(
      storage_interop.GetSignedPolicyOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        GetSignedPolicyOptions._fromJsObject(jsObject);
  }
}

class LifeCycleRule
    extends JsObjectWrapper<storage_interop.LifeCycleRuleJsImpl> {
  static final _expando = Expando<LifeCycleRule>();

  factory LifeCycleRule({
    storage_interop.LifeCycleRuleAction action,
    Map<String, dynamic> condition,
    String storageClass,
  }) =>
      LifeCycleRule._fromJsObject(storage_interop.LifeCycleRuleJsImpl(
        action: action,
        condition: jsify(condition),
        storageClass: storageClass,
      ));

  LifeCycleRule._fromJsObject(storage_interop.LifeCycleRuleJsImpl jsObject)
      : super.fromJsObject(jsObject);

  storage_interop.LifeCycleRuleAction get action => jsObject.action;
  set action(storage_interop.LifeCycleRuleAction v) => jsObject.action = v;
  Map<String, dynamic> get condition => dartify(jsObject.condition);

  set condition(Map<String, dynamic> v) => jsObject.condition = jsify(v);
  String get storageClass => jsObject.storageClass;

  set storageClass(String v) => jsObject.storageClass = v;
  static LifeCycleRule getInstance(
      storage_interop.LifeCycleRuleJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= LifeCycleRule._fromJsObject(jsObject);
  }
}

class Storage extends JsObjectWrapper<storage_interop.StorageJsImpl> {
  static final _expando = Expando<Storage>();

  Storage._fromJsObject(storage_interop.StorageJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Bucket bucket([String name]) {
    if (name != null) return Bucket.getInstance(jsObject.bucket(name));
    return Bucket.getInstance(jsObject.bucket());
  }

  static Storage getInstance(storage_interop.StorageJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Storage._fromJsObject(jsObject);
  }
}

class StorageNotification
    extends JsObjectWrapper<storage_interop.NotificationJsImpl> {
  static final _expando = Expando<StorageNotification>();

  StorageNotification._fromJsObject(storage_interop.NotificationJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Future<Map<String, dynamic>> delete(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.delete(options));
    } else {
      future = handleThenable(jsObject.delete());
    }
    return future.then(_listToMap);
  }

  Future<bool> exists(String topic, [Map<String, dynamic> options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.exist(topic, jsify(options)));
    } else {
      future = handleThenable(jsObject.exist(topic));
    }
    return future.then((value) => value.first as bool);
  }

  Future<StorageResponse<StorageNotification>> get(
      [storage_interop.GetBucketOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.get(options));
    } else {
      future = handleThenable(jsObject.get());
    }
    return _makeResponse(future, StorageNotification.getInstance);
  }

  Future<StorageResponse<Map<String, dynamic>>> getMetadata(
      [storage_interop.StorageBasicOptions options]) {
    Future<List> future;
    if (options != null) {
      future = handleThenable(jsObject.getMetadata(options));
    } else {
      future = handleThenable(jsObject.getMetadata());
    }
    return _makeResponse(future, (o) => dartify(o) as Map<String, dynamic>);
  }

  static StorageNotification getInstance(
      storage_interop.NotificationJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= StorageNotification._fromJsObject(jsObject);
  }
}

class StorageResponse<T> {
  final T data;
  final Map<String, dynamic> object;
  StorageResponse(this.data, this.object);
}

class StorageResponse0 {
  final Map<String, dynamic> apiResponse;
  StorageResponse0(this.apiResponse);
}

class UploadOptions
    extends JsObjectWrapper<storage_interop.UploadOptionsJsImpl> {
  static final _expando = Expando<UploadOptions>();

  factory UploadOptions({
    String configPath,
    Map<String, dynamic> metadata,
    String origin,
    num offset,
    String predefinedAcl,
    bool private,
    bool public,
    String uri,
    String userProject,
    String destination,
    String encryptionKey,
    String kmsKeyName,
    bool resumable,
  }) =>
      UploadOptions._fromJsObject(storage_interop.UploadOptionsJsImpl(
        configPath: configPath,
        metadata: jsify(metadata),
        origin: origin,
        offset: offset,
        predefinedAcl: predefinedAcl,
        private: private,
        public: public,
        uri: uri,
        userProject: userProject,
        destination: destination,
        encryptionKey: encryptionKey,
        kmsKeyName: kmsKeyName,
        resumable: resumable,
      ));

  UploadOptions._fromJsObject(storage_interop.UploadOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  String get configPath => jsObject.configPath;

  set configPath(String v) => jsObject.configPath = v;
  String get destination => jsObject.destination;

  set destination(String v) => jsObject.destination = v;
  String get encryptionKey => jsObject.encryptionKey;

  set encryptionKey(String v) => jsObject.encryptionKey = v;
  String get kmsKeyName => jsObject.kmsKeyName;

  set kmsKeyName(String v) => jsObject.kmsKeyName = v;
  Map<String, dynamic> get metadata => dartify(jsObject.metadata);

  set metadata(Map<String, dynamic> v) => jsObject.metadata = jsify(v);
  num get offset => jsObject.offset;

  set offset(num v) => jsObject.offset = v;
  String get origin => jsObject.origin;

  set origin(String v) => jsObject.origin = v;
  String get predefinedAcl => jsObject.predefinedAcl;

  set predefinedAcl(String v) => jsObject.predefinedAcl = v;
  bool get private => jsObject.private;

  set private(bool v) => jsObject.private = v;
  bool get public => jsObject.public;

  set public(bool v) => jsObject.public = v;
  bool get resumable => jsObject.resumable;

  set resumable(bool v) => jsObject.resumable = v;
  String get uri => jsObject.uri;

  set uri(String v) => jsObject.uri = v;
  String get userProject => jsObject.userProject;

  set userProject(String v) => jsObject.userProject = v;
  static UploadOptions getInstance(
      storage_interop.UploadOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= UploadOptions._fromJsObject(jsObject);
  }
}

abstract class _Bucket {
  Future<Map<String, dynamic>> addLifecycleRule(
    LifeCycleRule rule, [
    storage_interop.AddLifecycleRuleOptions options,
  ]);

  Future<StorageResponse<File>> combine(
    List /*String|File*/ sources,
    dynamic /*String,File*/ destination, [
    storage_interop.CombineOptions options,
  ]);

  Future<StorageResponse<Bucket>> create([
    CreateBucketRequest metadata,
  ]);

  Future<StorageResponse<Channel>> createChannel(
    String id,
    storage_interop.CreateChannelConfig config, [
    storage_interop.StorageBasicOptions options,
  ]);

  Future<StorageResponse<StorageNotification>> createNotification(
    String topic, [
    CreateNotificationOptions options,
  ]);

  Future<Map<String, dynamic>> delete(
    storage_interop.StorageBasicOptions options,
  );

  Future<void> deleteFiles([
    storage_interop.DeleteFilesOptions options,
  ]);

  Future<Map<String, dynamic>> deleteLabels(List<String> labels);

  Future<Map<String, dynamic>> disableRequesterPays();

  Future<Map<String, dynamic>> enableLogging(EnableLoggingOptions options);

  Future<Map<String, dynamic>> enableRequesterPays();

  Future<bool> exists([storage_interop.BucketExistsOptions options]);

  File file(String name, [storage_interop.FileOptions options]);

  Future<StorageResponse<Bucket>> get(
      [storage_interop.GetBucketOptions options]);

  Future<List<File>> getFiles([storage_interop.GetFilesOptions options]);

  Future<Map<String, dynamic>> getLabels(
      [storage_interop.StorageBasicOptions options]);

  Future<StorageResponse<Map<String, dynamic>>> getMetadata(
      [storage_interop.StorageBasicOptions options]);

  Future<StorageResponse<List<StorageNotification>>> getNotifications(
      [storage_interop.StorageBasicOptions options]);

  Future<String> getSignedUrl(GetBucketSignedUrlConfig config);

  Future<Map<String, dynamic>> lock(dynamic metageneration);

  Future<List<File>> makePrivate(
      [storage_interop.MakeBucketPrivateOptions options]);

  Future<List<File>> makePublic(
      [storage_interop.MakeBucketPublicOptions options]);

  StorageNotification notification(String id);

  Future<Map<String, dynamic>> removeRetentionPeriod();
  //TODO: implement setCorsConfiguration
  // Future setCorsConfiguration();

  Future<Map<String, dynamic>> setLabels(Map<String, String> labels,
      [storage_interop.StorageBasicOptions options]);

  Future<Map<String, dynamic>> setMetadata(Map<String, String> metadata,
      [storage_interop.StorageBasicOptions options]);

  Future<Map<String, dynamic>> setRetentionPeriod(int duration);

  Future<Map<String, dynamic>> setStorageClass(String storageClass,
      [storage_interop.StorageBasicOptions options]);

  void setUserProject(String userProject);

  Future<StorageResponse<File>> upload(String path, [UploadOptions options]);
}

abstract class _CreateWriteStreamOptions {}

abstract class _File {
  Future<StorageResponse<File>> copy(dynamic destination,
      [storage_interop.CopyOptions options]);

  Future<Readable> createReadStream(
      [storage_interop.CreateReadStreamOptions options]);

  Future<String> createResumableUpload([CreateResumableUploadOptions options]);

  Future<Writable> createWriteStream([CreateWriteStreamOptions options]);

  Future<Map<String, dynamic>> delete(
      [storage_interop.StorageBasicOptions options]);

  void deleteResumableCache();

  Future<Buffer> download([storage_interop.CreateReadStreamOptions options]);

  Future<bool> exists([storage_interop.StorageBasicOptions options]);

  Future<storage_interop.PolicyDocument> generateSignedPostPolicyV2(
      GetSignedPolicyOptions options);

  Future<StorageResponse<File>> get(
      [storage_interop.StorageBasicOptions options]);

  Future<StorageResponse<DateTime>> getExpirationDate();

  Future<StorageResponse<Map<String, dynamic>>> getMetadata(
      [storage_interop.StorageBasicOptions options]);

  Future<storage_interop.PolicyDocument> getSignedPolicy(
      GetSignedPolicyOptions options);

  Future<bool> isPublic();

  Future<Map<String, dynamic>> makePrivate(
      [storage_interop.MakeFilePrivateOptions options]);

  Future<Map<String, dynamic>> makePublic();

  Future<StorageResponse<File>> move(dynamic destination);

  Future<StorageResponse<File>> rotateEncryptionKey(
      [storage_interop.EncryptionKeyOptions options]);

  File setEncryptionKey(String encryptionKey);

  Future<Map<String, dynamic>> setMetadata(
      [Map<String, dynamic> metadata,
      storage_interop.StorageBasicOptions options]);

  Future<Map<String, dynamic>> setStorageClass(String storageClass,
      [storage_interop.StorageBasicOptions options]);

  void setUserProject(String userProject);
}
