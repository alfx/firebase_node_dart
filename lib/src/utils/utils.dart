// ignore_for_file: public_member_api_docs
import 'dart:async';

import 'package:js/js.dart';
import 'package:js/js_util.dart' as util;

import '../../admin.dart';
import '../admin/interop/admin_interop.dart';
import 'es6_interop.dart';
import 'js.dart';
import 'js_interop.dart' as js;

/// Calls [method] on JavaScript object [jsObject].
dynamic callMethod(Object jsObject, String method, List<dynamic> args) =>
    util.callMethod(jsObject, method, args);

bool _hasProperties(Object jsObject, List<String> properties) {
  return properties.every((element) => util.hasProperty(jsObject, element));
}

/// Returns Dart representation from JS Object.
dynamic dartify(Object jsObject) {
  if (_isBasicType(jsObject)) return jsObject;

  // Handle list
  if (jsObject is Iterable) return jsObject.map(dartify).toList();

  var jsDate = js.dartifyDate(jsObject);
  if (jsDate != null) return jsDate;

  if (_hasProperties(jsObject, ['firestore', 'id', 'parent'])) {
    // This is likely a document reference – at least we hope
    return DocumentReference.getInstance(jsObject);
  }

  if (_hasProperties(jsObject, ['latitude', 'longitude']) &&
      js.objectKeys(jsObject).length == 2) {
    // This is likely a GeoPoint – return it as-is
    return jsObject as GeoPoint;
  }

  var proto = util.getProperty(jsObject, '__proto__');
  if (_hasProperties(proto, ['toDate', 'toMillis'])) {
    return Timestamp.getInstance(jsObject);
  }

  // Assume a map then...
  return dartifyMap(jsObject);
}

Map<String, dynamic> dartifyMap(Object jsObject) {
  var keys = js.objectKeys(jsObject);
  var map = <String, dynamic>{};
  for (var key in keys) {
    map[key] = dartify(util.getProperty(jsObject, key));
  }
  return map;
}

Map<String, T> dartifyMapWithMapper<T>(
  Object jsObject,
  T Function(dynamic) mapper,
) {
  if (jsObject == null) return null;
  var keys = js.objectKeys(jsObject);
  var map = <String, T>{};
  for (var key in keys) {
    map[key] = mapper(util.getProperty(jsObject, key));
  }
  return map;
}

/// Handles the [Future] object with the provided [mapper] function.
PromiseJsImpl<S> handleFutureWithMapper<T, S>(
    Future<T> future, S Function(T) mapper) {
  return PromiseJsImpl<S>(allowInterop((
    void Function(S) resolve,
    void Function(Object) reject,
  ) {
    future.then((value) {
      var mappedValue = mapper(value);
      resolve(mappedValue);
    }).catchError(reject);
  }));
}

/// Handles the [PromiseJsImpl] object.
Future<T> handleThenable<T>(PromiseJsImpl<T> thenable) async {
  T value;
  try {
    value = await util.promiseToFuture(thenable);
  } on FirebaseErrorJsImpl catch (e) {
    throw FirebaseError(e);
  }
  return value;
}

// PromiseJsImpl handleFuture<T>(Future<T> future) {
//   return PromiseJsImpl(allowInterop((Function resolve) {
//     future.then(resolve);
//   }));
// }

/// Returns the JS implementation from Dart Object.
dynamic jsify(Object dartObject) {
  if (_isBasicType(dartObject)) return dartObject;

  // if (dartObject is Timestamp) return dartObject.jsObject;

  // if (dartObject is RemoteConfigParameterGroup) return dartObject.jsObject;

  // if (dartObject is RemoteConfigParameter) return dartObject.jsObject;

  if (dartObject is Iterable) return jsifyList(dartObject);

  if (dartObject is Map) return jsifyMap(dartObject);

  // if (dartObject is DocumentReference) return dartObject.jsObject;

  if (dartObject is FieldValue) return jsifyFieldValue(dartObject);

  if (dartObject is GeoPoint) return dartObject;

  if (dartObject is FieldPath) return dartObject;

  if (dartObject is Function) return allowInterop(dartObject);

  if (dartObject is ServiceAccount) return dartObject;

  if (dartObject is JsObjectWrapper) return dartObject.jsObject;

  throw ArgumentError.value(dartObject, 'dartObject', 'Could not convert');
}

dynamic jsifyList(Iterable list) => js.toJSArray(list.map(jsify).toList());

dynamic jsifyMap(Map dartObject) {
  var jsMap = util.newObject();
  for (final key in dartObject.keys) {
    final value = jsify(dartObject[key]);
    // jsMap.setProperty(key, value);
    util.setProperty(jsMap, key, value);
  }
  // dartObject.forEach((key, value) {
  //   jsMap.setProperty(key, jsify(value));
  // });
  return jsMap;
}

/// Resolves error.
void Function(Object) resolveError(Completer c) =>
    allowInterop(c.completeError);

/// Returns `true` if the [value] is a very basic built-in type - e.g.
/// `null`, [num], [bool] or [String]. It returns `false` in the other case.
bool _isBasicType(Object value) =>
    value == null || value is num || value is bool || value is String;

class FirebaseError extends Error implements FirebaseErrorJsImpl {
  final FirebaseErrorJsImpl _source;

  FirebaseError(this._source);

  @override
  String get code => _source.code;

  @override
  String get message => _source.message;

  // String get name => util.getProperty(_source, 'name');

  // Object get serverResponse => util.getProperty(_source, 'serverResponse');

  @override
  String get stack => _source.stack;

  @override
  String toString() => 'FirebaseError: $message ($code)';
}
