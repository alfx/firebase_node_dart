import 'dart:js_util';

import 'package:node_interop/http.dart';
import 'package:node_io/node_io.dart';

import '../utils/utils.dart';

/// HttpRequest used in [FunctionsBuilder.https]
class ExpressHttpRequest extends NodeHttpRequest {
  dynamic _body;

  /// HttpRequest used in [FunctionsBuilder.https]
  ExpressHttpRequest(
      IncomingMessage nativeRequest, ServerResponse nativeResponse)
      : super(nativeRequest, nativeResponse);

  /// Body of request that is being made.
  dynamic get body {
    if (!hasProperty(nativeInstance, 'body')) return null;
    return _body ??= dartify(getProperty(nativeInstance, 'body'));
  }
}
