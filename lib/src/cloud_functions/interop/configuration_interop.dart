// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.configuration;

import 'package:js/js.dart';

// @JS()
// @anonymous
// abstract class DeploymentOptionsJsImpl extends RuntimeOptions {
//   external dynamic get regions;
//   external set regions(/*List<String>*/ dynamic v);

//   external ScheduleJsImpl get schedule;
//   external set schedule(ScheduleJsImpl v);
// }

@JS()
@anonymous
abstract class RuntimeOptions {
  external factory RuntimeOptions({dynamic memory, num timeoutSeconds});

  external dynamic get memory;
  external set memory(dynamic v);

  external num get timeoutSeconds;
  external set timeoutSeconds(num v);
}

// @JS()
// @anonymous
// abstract class ScheduleJsImpl {
//   external ScheduleRetryConfig get retryConfig;
//   external set retryConfig(ScheduleRetryConfig v);
//   external String get schedule;
//   external set schedule(String v);
//   external String get timeZone;
//   external set timeZone(String v);
// }

@JS()
@anonymous
abstract class ScheduleRetryConfig {
  external String get maxBackoffDuration;
  external set maxBackoffDuration(String v);

  external num get maxDoublings;
  external set maxDoublings(num v);

  external String get maxRetryDuration;
  external set maxRetryDuration(String v);

  external String get minBackoffDuration;
  external set minBackoffDuration(String v);

  external num get retryCount;
  external set retryCount(num v);

  external factory ScheduleRetryConfig({
    String maxBackoffDuration,
    num maxDoublings,
    String maxRetryDuration,
    String minBackoffDuration,
    num retryCount,
  });
}
