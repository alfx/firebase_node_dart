// ignore_for_file: public_member_api_docs, non_constant_identifier_names
@JS()
library firebase_node.functions.interop;

import 'package:js/js.dart';

import 'analytics_interop.dart';
import 'auth_interop.dart';
import 'configuration_interop.dart';
import 'crashlytics_interop.dart';
import 'database_interop.dart';
import 'firestore_interop.dart';
import 'https_interop.dart';
import 'pubsub_interop.dart';
import 'remote_config_interop.dart';
import 'storage_interop.dart';
import 'test_lab_interop.dart';

@JS('firebaseFunctions')
external FunctionsBuilderJsImpl get functionsBuilderJsImpl;

@JS('firebaseFunctions.MAX_TIMEOUT_SECONDS')
external num get MAX_TIMEOUT_SECONDS;

@JS('firebaseFunctions.MIN_TIMEOUT_SECONDS')
external num get MIN_TIMEOUT_SECONDS;

@JS('firebaseFunctions.SUPPORTED_REGIONS')
external List /*String*/ get SUPPORTED_REGIONS;

@JS('firebaseFunctions.VALID_MEMORY_OPTIONS')
external List /*String*/ get VALID_MEMORY_OPTIONS;

@JS('firebaseFunctions.config')
external dynamic config();

typedef JsHandler<T> = Function(T, EventContextJsImpl);

@JS()
@anonymous
abstract class AuthContextJsImpl {
  external dynamic get token;
  external set token(dynamic v);

  external String get uid;
  external set uid(String v);
}

@JS()
@anonymous
abstract class ChangeJsImpl<T> {
  external factory ChangeJsImpl({T before, T after});
  external T get after;
  external T get before;
}

@JS()
@anonymous
abstract class EventContextJsImpl {
  ///Authentication information for the user that triggered the function. This
  ///object contains uid and token properties for authenticated users. For more
  ///detail including token keys, see the security rules reference.
  ///
  ///This field is only populated for Realtime Database triggers and Callable
  ///functions. For an unauthenticated user, this field is null. For Firebase
  ///admin users and event types that do not provide user information, this
  ///field does not exist.
  external AuthContextJsImpl get auth;

  ///The level of permissions for a user. Valid values are:
  ///- ADMIN Developer user or user authenticated via a service account.
  ///- USER Known user.
  ///- UNAUTHENTICATED Unauthenticated action
  ///- null For event types that do not provide user information (all except
  ///Realtime Database).
  external String get authType;

  ///The event’s unique identifier.
  external String get eventId;

  ///Type of event. Valid values are:
  ///- providers/google.firebase.analytics/eventTypes/event.log
  ///- providers/firebase.auth/eventTypes/user.create
  ///- providers/firebase.auth/eventTypes/user.delete
  ///- providers/firebase.crashlytics/eventTypes/issue.new
  ///- providers/firebase.crashlytics/eventTypes/issue.regressed
  ///- providers/firebase.crashlytics/eventTypes/issue.velocityAlert
  ///- providers/google.firebase.database/eventTypes/ref.write
  ///- providers/google.firebase.database/eventTypes/ref.create
  ///- providers/google.firebase.database/eventTypes/ref.update
  ///- providers/google.firebase.database/eventTypes/ref.delete
  ///- providers/cloud.firestore/eventTypes/document.write
  ///- providers/cloud.firestore/eventTypes/document.create
  ///- providers/cloud.firestore/eventTypes/document.update
  ///- providers/cloud.firestore/eventTypes/document.delete
  ///- google.pubsub.topic.publish
  ///- google.storage.object.finalize
  ///- google.storage.object.archive
  ///- google.storage.object.delete
  ///- google.storage.object.metadataUpdate
  ///- google.firebase.remoteconfig.update
  external String get eventType;

  ///An object containing the values of the wildcards in the path parameter
  ///provided to the ref() method for a Realtime Database trigger. Cannot be
  ///accessed while inside the handler namespace.
  external dynamic get params;

  ///The resource that emitted the event. Valid values are:
  ///
  ///- Analytics — projects/<projectId>/events/<analyticsEventType>
  ///- Realtime Database — projects/_/instances/<databaseInstance>/refs/
  ///<databasePath>
  ///- Storage — projects/_/buckets/<bucketName>/objects/<fileName>#<generation>
  ///- Authentication — projects/<projectId>
  ///- Pub/Sub — projects/<projectId>/topics/<topicName>
  ///
  ///Because Realtime Database instances and Cloud Storage buckets are
  ///globally unique and not tied to the project, their resources start with
  ///projects/_. Underscore is not a valid project name.
  external ResourceJsImpl get resource;

  ///Timestamp for the event as an [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) string.
  external String get timestamp;
}

@JS()
@anonymous
abstract class FunctionsBuilderJsImpl {
  external AnalyticsFunctionsBuilderJsImpl get analytics;
  external AuthFunctionsBuilderJsImpl get auth;
  external CrashlyticFunctionsBuilderJsImpl get crashlytic;
  external DatabaseFunctionsBuilderJsImpl get database;
  external FirestoreFunctionsBuilderJsImpl get firestore;
  external HttpsFunctionsBuilderJsImpl get https;
  external PubsubFunctionsBuilderJsImpl get pubsub;
  external RemoteConfigFunctionsBuilderJsImpl get remoteConfig;
  external StorageFunctionsBuilderJsImpl get storage;
  external TestLabFunctionsBuilderJsImpl get testLab;
  // external dynamic config();

  ///Configure the regions that the function is deployed to.
  ///
  ///Example:
  ///- functions.region('us-east1')
  ///- functions.region('us-east1', 'us-central1')
  external FunctionsBuilderJsImpl region(/*List<String>*/ dynamic regions);

  ///Configure runtime options for the function.
  external FunctionsBuilderJsImpl runWith(RuntimeOptions options);
}

@JS()
@anonymous
abstract class ResourceJsImpl {
  external dynamic get labels;
  external set labels(dynamic v);

  external String get name;
  external set name(String v);

  external String get service;
  external set service(String v);

  external String get type;
  external set type(String v);
}
