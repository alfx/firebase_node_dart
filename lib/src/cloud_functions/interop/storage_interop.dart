// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.storage;

import 'package:js/js.dart';

import 'functions_interop.dart';

@JS()
@anonymous
abstract class BucketBuilderJsImpl {
  ///Event handler which fires every time a Google Cloud Storage change occurs.
  external ObjectBuilderJsImpl object();
}

@JS()
@anonymous
abstract class CustomerEncryption {
  ///The encryption algorithm that was used. Always contains the value AES256.
  external String get encryptionAlgorithm;

  ///An RFC 4648 base64-encoded string of the SHA256 hash of your encryption
  ///key. You can use this SHA256 hash to uniquely identify the AES-256
  ///encryption key required to decrypt the object, which you must store
  ///securely.
  external String get keySha256;
}

@JS()
@anonymous
abstract class ObjectBuilderJsImpl {
  ///Event handler sent only when a bucket has enabled object versioning. This
  ///event indicates that the live version of an object has become an archived
  ///version, either because it was archived or because it was overwritten by
  ///the upload of an object of the same name.
  external dynamic onArchieve(JsHandler<ObjectMetadataJsImpl> handler);

  ///Event handler which fires every time a Google Cloud Storage deletion
  ///occurs.
  ///
  ///Sent when an object has been permanently deleted. This includes objects
  ///that are overwritten or are deleted as part of the bucket's lifecycle
  ///configuration. For buckets with object versioning enabled, this is not
  ///sent when an object is archived, even if archival occurs via the storage.
  ///objects.delete method.
  external dynamic onDelete(JsHandler<ObjectMetadataJsImpl> handler);

  ///Event handler which fires every time a Google Cloud Storage object
  ///creation occurs.
  ///
  ///Sent when a new object (or a new generation of an existing object) is
  ///successfully created in the bucket. This includes copying or rewriting an
  ///existing object. A failed upload does not trigger this event.
  external dynamic onFinalize(JsHandler<ObjectMetadataJsImpl> handler);

  ///Event handler which fires every time the metadata of an existing object
  ///changes.
  external dynamic onMetadataUpdate(JsHandler<ObjectMetadataJsImpl> handler);
}

@JS()
@anonymous
abstract class ObjectMetadataJsImpl {
  external dynamic get acl;

  ///Storage bucket that contains the object.
  external String get bucket;

  ///The value of the Cache-Control header, used to determine whether Internet
  ///caches are allowed to cache public data for an object.
  external String get cacheControl;

  ///Specifies the number of originally uploaded objects from which a composite
  ///object was created.
  external String get componentCount;

  ///The value of the Content-Disposition header, used to specify presentation
  ///information about the data being transmitted.
  external String get contentDisposition;

  ///Content-Encoding to indicate that an object is compressed (for example,
  ///with gzip compression) while maintaining its Content-Type.
  external String get contentEncoding;

  ///ISO 639-1 language code of the content.
  external String get contentLanguage;

  ///The object's content type, also known as the MIME type.
  external String get contentType;

  ///The object's CRC32C hash. All Google Cloud Storage objects have a CRC32C
  ///hash or MD5 hash.
  external String get crc32c;

  ///Customer-supplied encryption key.
  ///
  ///This object contains the following properties:
  ///
  ///`encryptionAlgorithm (string|null)`: The encryption algorithm that was
  ///used. Always contains the value AES256.
  ///
  ///`keySha256 (string|mull)`: An RFC 4648 base64-encoded string of the
  ///SHA256 hash of your encryption key. You can use this SHA256 hash to
  ///uniquely identify the AES-256 encryption key required to decrypt the
  ///object, which you must store securely.
  external CustomerEncryption get customerEncryption;

  external String get etag;

  ///Generation version number that changes each time the object is overwritten.
  external String get generation;

  ///The ID of the object, including the bucket name, object name, and
  ///generation number.
  external String get id;

  ///The kind of the object, which is always storage#object.
  external String get kind;

  ///MD5 hash for the object. All Google Cloud Storage objects have a CRC32C
  ///hash or MD5 hash.
  external String get md5Hash;

  ///Media download link.
  external String get mediaLink;

  ///User-provided metadata.
  external dynamic get metadata;

  ///Meta-generation version number that changes each time the object's
  ///metadata is updated.
  external String get metageneration;

  ///The object's name.
  external String get name;

  external ObjectOwner get owner;

  ///Link to access the object, assuming you have sufficient permissions.
  external String get selfLink;

  ///The value of the Content-Length header, used to determine the length of
  ///the object data in bytes.
  external String get size;

  ///Storage class of the object.
  external String get storageClass;

  ///The creation time of the object in RFC 3339 format.
  external String get timeCreated;

  ///The deletion time of the object in RFC 3339 format. Returned only if this
  ///version of the object has been deleted.
  external String get timeDeleted;

  external String get timeStorageClassUpdated;

  ///The modification time of the object metadata in RFC 3339 format.
  external String get updated;
}

@JS()
@anonymous
abstract class ObjectOwner {
  external String get entity;
  external String get entityId;
}

@JS()
@anonymous
abstract class StorageFunctionsBuilderJsImpl {
  ///The optional bucket function allows you to choose which buckets' events to
  ///handle. This step can be bypassed by calling object() directly, which will
  ///use the default Cloud Storage for Firebase bucket.
  external BucketBuilderJsImpl bucket(String bucket);

  ///Handle events related to Cloud Storage objects.
  external ObjectBuilderJsImpl object();
}
