// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.firestore;

import 'package:js/js.dart';

import '../../admin/interop/firestore_interop.dart' as admin;
import 'functions_interop.dart';

@JS()
@anonymous
abstract class DocumentBuilderJsImpl {
  ///Respond only to document creations.
  external dynamic onCreate(JsHandler<admin.DocumentSnapshotJsImpl> handler);

  ///Respond only to document deletions.
  external dynamic onDelete(JsHandler<admin.DocumentSnapshotJsImpl> handler);

  ///Respond only to document updates.
  external dynamic onUpdate(
      JsHandler<ChangeJsImpl<admin.DocumentSnapshotJsImpl>> handler);

  ///Respond to all document writes (creates, updates, or deletes).
  external dynamic onWrite(
      JsHandler<ChangeJsImpl<admin.DocumentSnapshotJsImpl>> handler);
}

@JS()
@anonymous
abstract class FirestoreFunctionsBuilderJsImpl {
  ///Select the Firestore document to listen to for events.
  external DocumentBuilderJsImpl document(String path);
}
