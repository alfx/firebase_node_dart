// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.crashlytic;

import 'package:js/js.dart';

import 'functions_interop.dart';

@JS()
@anonymous
abstract class CrashlyticAppInfo {
  ///Unique application identifier within an app store, either the Android
  ///package name or the iOS bundle id.
  external String get appId;

  ///The app's name.
  external String get appName;

  ///The app's platform.
  external String get appPlatform;

  ///The app's version name.
  external String get latestAppVersion;
}

@JS()
@anonymous
abstract class CrashlyticFunctionsBuilderJsImpl {
  ///Handle events related to Crashlytics issues. An issue in Crashlytics is an
  ///aggregation of crashes which have a shared root cause.
  external IssueBuilderJsImpl issue();
}

@JS()
@anonymous
abstract class Issue {
  ///AppInfo interface describing the App.
  external CrashlyticAppInfo get appInfo;

  ///UTC when the issue occurred in ISO8601 standard representation.
  ///
  ///Example: 1970-01-17T10:52:15.661-08:00
  external String get createTime;

  ///Crashlytics-provided issue ID.
  external String get issueId;

  ///Crashlytics-provided issue title.
  external String get issueTitle;

  ///UTC When the issue was closed in ISO8601 standard representation.
  ///
  ///Example: 1970-01-17T10:52:15.661-08:00
  external String get resolvedTime;

  ///Information about the velocity alert, like number of crashes and percentage
  ///of users affected by the issue.
  external VelocityAlert get velocityAlert;
}

@JS()
@anonymous
abstract class IssueBuilderJsImpl {
  ///Event handler that fires every time a new issue occurs in a project.
  external dynamic onNew(JsHandler<Issue> handler);

  ///Event handler that fires every time a regressed issue reoccurs in a
  ///project.
  external dynamic onRegressed(JsHandler<Issue> handler);

  ///Event handler that fires every time a velocity alert occurs in a project.
  external dynamic onVelocityAlert(JsHandler<Issue> handler);
}

@JS()
@anonymous
abstract class VelocityAlert {
  ///The number of crashes that this issue has caused.
  external num get crashes;

  ///The percentage of sessions which have been impacted by this issue.
  ///
  ///Example: .04
  external num get crashPercentage;
}
