// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.remote_config;

import 'package:js/js.dart';

import 'functions_interop.dart';

@JS()
@anonymous
abstract class RemoteConfigFunctionsBuilderJsImpl {
  ///Handle all updates (including rollbacks) that affect a Remote Config
  ///project.
  external dynamic onUpdate(JsHandler<TemplateVersion> handler);
}

@JS()
@anonymous
abstract class RemoteConfigUser {
  ///Email address of the Remote Config account that performed the update.
  external String get email;

  ///Email address of the Remote Config account that performed the update.
  external String get imageUrl;

  ///Email address of the Remote Config account that performed the update.
  external String get name;
}

@JS()
@anonymous
abstract class TemplateVersion {
  ///A description associated with this Remote Config template version.
  external String get description;

  ///The version number of the Remote Config template that this update rolled
  ///back to. Only applies if this update was a rollback.
  external num get rollbackSource;

  ///The origin of the caller - either the Firebase console or the Remote
  ///Config REST API. See RemoteConfigUpdateOrigin for valid values.
  external String get updateOrigin;

  ///When the template was updated in format (ISO8601 timestamp).
  external String get updateTime;

  //TODO: mayble implement enum
  ///The type of update action that was performed, whether forced, incremental,
  ///or a rollback operation. See RemoteConfigUpdateType for valid values.
  external String get updateType;

  ///Metadata about the account that performed the update, of type
  ///`RemoteConfigUser`.
  external RemoteConfigUser get updateUser;

  ///The version number of the updated Remote Config template.
  external num get versionNumber;
}
