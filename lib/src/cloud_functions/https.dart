import 'package:js/js.dart';
import 'package:meta/meta.dart';
import 'package:node_interop/http.dart';

import '../utils/js.dart';
import '../utils/utils.dart';
import 'functions.dart';
import 'interop/https_interop.dart' as interop;

///The interface for metadata for the API as passed to the handler.
class CallableContext extends JsObjectWrapper<interop.CallableContextJsImpl> {
  CallableContext._fromJsObject(interop.CallableContextJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///The result of decoding and verifying a Firebase Auth ID token.
  AuthContext get auth => AuthContext.getInstance(jsObject.auth);

  ///An unverified token for a Firebase Instance ID.
  String get instanceIdToken => jsObject.instanceIdToken;

  ///The raw request handled by the callable.
  IncomingMessage get rawRequest => jsObject.rawRequest;
}

// enum FunctionsErrorCode {
//   ok,
//   cancelled,
//   unknown,
//   invalidArgument,
//   deadlineExceeded,
//   notFound,
//   alreadyExists,
//   permissionDenied,
//   resourceExhausted,
//   failedPrecondition,
//   aborted,
//   outOfRange,
//   unimplemented,
//   internal,
//   unavailable,
//   dataLoss,
//   unauthenticated,
// }

/// An explicit error that can be thrown from a handler to send an error to the
/// client that called the function.
class HttpsError extends JsObjectWrapper<interop.HttpsErrorJsImpl> {
  /// An explicit error that can be thrown from a handler to send an error to
  /// the client that called the function.
  factory HttpsError({
    @required String code,
    @required Map<String, dynamic> details,
    @required String message,
    @required String name,
    String stack,
  }) {
    if (stack == null) {
      return HttpsError._fromJsObject(interop.HttpsErrorJsImpl(
        code: code,
        details: jsify(details),
        message: message,
        name: name,
      ));
    } else {
      return HttpsError._fromJsObject(interop.HttpsErrorJsImpl(
        code: code,
        details: jsify(details),
        message: message,
        name: name,
        stack: stack,
      ));
    }
  }

  HttpsError._fromJsObject(interop.HttpsErrorJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///A standard error code that will be returned to the client. This also
  ///determines the HTTP status code of the response, as defined in code.proto.
  String get code => jsObject.code;

  ///Extra data to be converted to JSON and included in the error response.
  Map<String, dynamic> get details => dartify(jsObject.details);

  ///Error message
  String get message => jsObject.message;

  ///Error name
  String get name => jsObject.name;

  ///Error stack
  String get stack => jsObject.stack;
}

///Construct https functions
class HttpsFunctionsBuilder
    extends JsObjectWrapper<interop.HttpsFunctionsBuilderJsImpl> {
  ///Construct https functions
  HttpsFunctionsBuilder.fromJsObject(
      interop.HttpsFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Declares a callable method for clients to call using a Firebase SDK.
  dynamic onCall(HttpsOnCall handler) {
    return jsObject.onCall(allowInterop((dataJsImpl, contextJsImpl) {
      final data = dartify(dataJsImpl) as Map<String, dynamic>;
      final context = CallableContext._fromJsObject(contextJsImpl);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Handle HTTP requests.
  dynamic onRequest(HttpsOnRequest handler) {
    return jsObject.onRequest(allowInterop((req, res) {
      return httpsFunction(req, res, handler);
    }));
  }
}
