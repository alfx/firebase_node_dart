import 'package:js/js.dart';

import '../../admin.dart';
import '../utils/js.dart';
import 'functions.dart';
import 'interop/firestore_interop.dart' as interop;

///Construct firestore dcoument functions
class DocumentBuilder extends JsObjectWrapper<interop.DocumentBuilderJsImpl> {
  static final _expando = Expando<DocumentBuilder>();

  DocumentBuilder._fromJsObject(interop.DocumentBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Respond only to document creations.
  dynamic onCreate(CloudFunction<DocumentSnapshot> handler) {
    return jsObject.onCreate(allowInterop((snapshotJs, contextJs) {
      return cloudFunction(
        DocumentSnapshot.getInstance(snapshotJs),
        EventContext.fromJsObject(contextJs),
        handler,
      );
    }));
  }

  ///Respond only to document deletions.
  dynamic onDelete(CloudFunction<DocumentSnapshot> handler) {
    return jsObject.onDelete(allowInterop((snapshotJs, contextJs) {
      return cloudFunction(
        DocumentSnapshot.getInstance(snapshotJs),
        EventContext.fromJsObject(contextJs),
        handler,
      );
    }));
  }

  ///Respond only to document updates.
  dynamic onUpdate(CloudFunction<Change<DocumentSnapshot>> handler) {
    return jsObject.onUpdate(allowInterop((dataJs, contextJs) {
      final data = Change(
        after: DocumentSnapshot.getInstance(dataJs.after),
        before: DocumentSnapshot.getInstance(dataJs.before),
      );
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Respond to all document writes (creates, updates, or deletes).
  dynamic onWrite(CloudFunction<Change<DocumentSnapshot>> handler) {
    return jsObject.onWrite(allowInterop((dataJs, contextJs) {
      final data = Change(
        after: DocumentSnapshot.getInstance(dataJs.after),
        before: DocumentSnapshot.getInstance(dataJs.before),
      );
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Construct firestore dcoument functions
  static DocumentBuilder getInstance(interop.DocumentBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= DocumentBuilder._fromJsObject(jsObject);
  }
}

///Construct firestore functions
class FirestoreFunctionsBuilder
    extends JsObjectWrapper<interop.FirestoreFunctionsBuilderJsImpl> {
  static final _expando = Expando<FirestoreFunctionsBuilder>();

  FirestoreFunctionsBuilder._fromJsObject(
      interop.FirestoreFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Select the Firestore document to listen to for events.
  DocumentBuilder document(String path) =>
      DocumentBuilder.getInstance(jsObject.document(path));

  ///Construct firestore functions
  static FirestoreFunctionsBuilder getInstance(
      interop.FirestoreFunctionsBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        FirestoreFunctionsBuilder._fromJsObject(jsObject);
  }
}
