import 'package:js/js.dart';

import '../utils/js.dart';
import '../utils/utils.dart';
import 'functions.dart';
import 'interop/test_lab_interop.dart' as interop;

class ClientInfo extends JsObjectWrapper<interop.ClientInfoJsImpl> {
  ClientInfo._fromJsObject(interop.ClientInfoJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, dynamic> get details => dartify(jsObject.details);

  String get name => jsObject.name;
}

class TestLabFunctionsBuilder
    extends JsObjectWrapper<interop.TestLabFunctionsBuilderJsImpl> {
  TestLabFunctionsBuilder.fromJsObject(
      interop.TestLabFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  TestMatrixBuilder testMatrix() =>
      TestMatrixBuilder._fromJsObject(jsObject.testMatrix());
}

class TestMatrix extends JsObjectWrapper<interop.TestMatrixJsImpl> {
  TestMatrix._fromJsObject(interop.TestMatrixJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ClientInfo get clientInfo => ClientInfo._fromJsObject(jsObject.clientInfo);

  String get createTime => jsObject.createTime;

  String get invalidMatrixDetails => jsObject.invalidMatrixDetails;

  String get outcomeSummary => jsObject.outcomeSummary;

  String get state => jsObject.state;

  String get testMatrixId => jsObject.testMatrixId;
}

class TestMatrixBuilder
    extends JsObjectWrapper<interop.TestMatrixBuilderJsImpl> {
  TestMatrixBuilder._fromJsObject(interop.TestMatrixBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  dynamic onComplete(CloudFunction<TestMatrix> handler) {
    return jsObject.onComplete(allowInterop((testMatrixJsImpl, contextJsImpl) {
      final data = TestMatrix._fromJsObject(testMatrixJsImpl);
      final context = EventContext.fromJsObject(contextJsImpl);
      return cloudFunction(data, context, handler);
    }));
  }
}
