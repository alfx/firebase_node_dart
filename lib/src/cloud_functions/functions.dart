import 'dart:async';

import 'package:js/js_util.dart';
import 'package:node_interop/https.dart';

import '../utils/js.dart';
import '../utils/js_interop.dart';
import '../utils/utils.dart';
import 'analytics.dart';
import 'auth.dart';
import 'crashlytics.dart';
import 'database.dart';
import 'express_http.dart';
import 'firestore.dart';
import 'https.dart';
import 'interop/configuration_interop.dart';
import 'interop/functions_interop.dart' as interop;
import 'interop/functions_interop.dart';
import 'pubsub.dart';
import 'remote_config.dart';
import 'storage.dart';
import 'test_lab.dart';

///Interface to register or create firebase functions.
final FunctionsModuleBuilder functions =
    FunctionsModuleBuilder._fromJsObject(functionsBuilderJsImpl);

dynamic cloudFunction<T, S>(
  T data,
  S context,
  FutureOr Function(T, S) handler,
) {
  final result = handler(data, context);
  if (result is Future) {
    return handleFutureWithMapper(result, jsify);
  } else {
    return jsify(result);
  }
}

dynamic httpsFunction(
  IncomingMessage req,
  ServerResponse res,
  HttpsOnRequest handler,
) {
  final result = handler(ExpressHttpRequest(req, res));
  if (result is Future) {
    return handleFutureWithMapper(result, jsify);
  } else {
    return jsify(result);
  }
}

typedef CloudFunction<T> = FutureOr Function(T data, EventContext context);
typedef HttpsOnCall = FutureOr Function(Map<String, dynamic>, CallableContext);
typedef HttpsOnRequest = FutureOr Function(ExpressHttpRequest);

class AuthContext extends JsObjectWrapper<interop.AuthContextJsImpl> {
  static final _expando = Expando<AuthContext>();

  static AuthContext getInstance(interop.AuthContextJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= AuthContext._fromJsObject(jsObject);
  }

  AuthContext._fromJsObject(interop.AuthContextJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, dynamic> get token => dartify(jsObject.token);

  String get uid => jsObject.uid;
}

///The Functions interface for events that change state, such as Realtime
///Database or Cloud Firestore onWrite and onUpdate.
///
///For more information about the format used to construct Change objects, see
///[cloud-functions.ChangeJson](https://firebase.google.com/docs/reference/functions/cloud_functions_.changejson).

class Change<T> {
  final T after;
  final T before;

  ///The Functions interface for events that change state, such as Realtime
  ///Database or Cloud Firestore onWrite and onUpdate.
  ///
  ///For more information about the format used to construct Change objects, see
  ///[cloud-functions.ChangeJson](https://firebase.google.com/docs/reference/functions/cloud_functions_.changejson).
  const Change({this.after, this.before});
}

/// The context in which an event occurred.
///
/// An EventContext describes:
///
/// The time an event occurred.
/// A unique identifier of the event.
/// The resource on which the event occurred, if applicable.
/// Authorization of the request that triggered the event, if applicable and
/// available.
class EventContext extends JsObjectWrapper<interop.EventContextJsImpl> {
  // static final _expando = Expando<EventContext>();

  /// The context in which an event occurred.
  ///
  /// An EventContext describes:
  ///
  /// The time an event occurred.
  /// A unique identifier of the event.
  /// The resource on which the event occurred, if applicable.
  /// Authorization of the request that triggered the event, if applicable and
  /// available.
  // factory EventContext.getInstance(interop.EventContextJsImpl jsObject) {
  //   if (jsObject == null) return null;
  //   return _expando[jsObject] ??= EventContext._fromJsObject(jsObject);
  // }

  EventContext.fromJsObject(interop.EventContextJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Authentication information for the user that triggered the function. This
  ///object contains uid and token properties for authenticated users. For more
  ///detail including token keys, see the security rules reference.
  ///
  ///This field is only populated for Realtime Database triggers and Callable
  ///functions. For an unauthenticated user, this field is null. For Firebase
  ///admin users and event types that do not provide user information, this
  ///field does not exist.
  AuthContext get auth => AuthContext._fromJsObject(jsObject.auth);

  ///The level of permissions for a user. Valid values are:
  ///- ADMIN Developer user or user authenticated via a service account.
  ///- USER Known user.
  ///- UNAUTHENTICATED Unauthenticated action
  ///- null For event types that do not provide user information (all except
  ///Realtime Database).
  String get authType => jsObject.authType;

  ///The event’s unique identifier.
  String get eventId => jsObject.eventId;

  ///Type of event. Valid values are:
  ///- providers/google.firebase.analytics/eventTypes/event.log
  ///- providers/firebase.auth/eventTypes/user.create
  ///- providers/firebase.auth/eventTypes/user.delete
  ///- providers/firebase.crashlytics/eventTypes/issue.new
  ///- providers/firebase.crashlytics/eventTypes/issue.regressed
  ///- providers/firebase.crashlytics/eventTypes/issue.velocityAlert
  ///- providers/google.firebase.database/eventTypes/ref.write
  ///- providers/google.firebase.database/eventTypes/ref.create
  ///- providers/google.firebase.database/eventTypes/ref.update
  ///- providers/google.firebase.database/eventTypes/ref.delete
  ///- providers/cloud.firestore/eventTypes/document.write
  ///- providers/cloud.firestore/eventTypes/document.create
  ///- providers/cloud.firestore/eventTypes/document.update
  ///- providers/cloud.firestore/eventTypes/document.delete
  ///- google.pubsub.topic.publish
  ///- google.storage.object.finalize
  ///- google.storage.object.archive
  ///- google.storage.object.delete
  ///- google.storage.object.metadataUpdate
  ///- google.firebase.remoteconfig.update
  String get eventType => jsObject.eventType;

  ///An object containing the values of the wildcards in the path parameter
  ///provided to the ref() method for a Realtime Database trigger. Cannot be
  ///accessed while inside the handler namespace.
  Map<String, dynamic> get params => dartify(jsObject.params);

  ///The resource that emitted the event. Valid values are:
  ///
  ///- Analytics — projects/<projectId>/events/<analyticsEventType>
  ///- Realtime Database — projects/_/instances/<databaseInstance>/refs/
  ///<databasePath>
  ///- Storage — projects/_/buckets/<bucketName>/objects/<fileName>#<generation>
  ///- Authentication — projects/<projectId>
  ///- Pub/Sub — projects/<projectId>/topics/<topicName>
  ///
  ///Because Realtime Database instances and Cloud Storage buckets are
  ///globally unique and not tied to the project, their resources start with
  ///projects/_. Underscore is not a valid project name.
  Resource get resource => Resource.getInstance(jsObject.resource);

  ///Timestamp for the event as an [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) string.
  String get timestamp => jsObject.timestamp;
}

///Interface for firebase functions
class FunctionsBuilder extends JsObjectWrapper<FunctionsBuilderJsImpl> {
  static final _expando = Expando<FunctionsBuilder>();

  ///Interface for firebase functions
  static FunctionsBuilder getInstance(FunctionsBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] = FunctionsBuilder._fromJsObject(jsObject);
  }

  FunctionsBuilder._fromJsObject(FunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Interface for analytics functions
  AnalyticsFunctionsBuilder get analytics =>
      AnalyticsFunctionsBuilder.fromJsObject(jsObject.analytics);

  ///Interface for auth functions
  AuthFunctionsBuilder get auth =>
      AuthFunctionsBuilder.fromJsObject(jsObject.auth);

  ///Interface for crashlytic functions
  CrashlyticsFunctionsBuilder get crashlytic =>
      CrashlyticsFunctionsBuilder.fromJsObject(jsObject.crashlytic);

  ///Interface for database functions
  DatabaseFunctionsBuilder get database =>
      DatabaseFunctionsBuilder.fromJsObject(jsObject.database);

  ///Interface for firestore functions
  FirestoreFunctionsBuilder get firestore =>
      FirestoreFunctionsBuilder.getInstance(jsObject.firestore);

  ///Interface for https functions
  HttpsFunctionsBuilder get https =>
      HttpsFunctionsBuilder.fromJsObject(jsObject.https);

  ///Interface for pubsub functions
  PubsubFunctionsBuilder get pubsub =>
      PubsubFunctionsBuilder.fromJsObject(jsObject.pubsub);

  ///Interface for remoteConfig functions
  RemoteConfigFunctionsBuilder get remoteConfig =>
      RemoteConfigFunctionsBuilder.fromJsObject(jsObject.remoteConfig);

  ///Interface for storage functions
  StorageFunctionsBuilder get storage =>
      StorageFunctionsBuilder.fromJsObject(jsObject.storage);

  ///Interface for testLab functions
  TestLabFunctionsBuilder get testLab =>
      TestLabFunctionsBuilder.fromJsObject(jsObject.testLab);

  ///Register a function
  void operator []=(String key, dynamic function) =>
      setProperty(exports, key, function);

  ///Configure the regions that the function is deployed to.
  ///
  ///Example:
  ///- functions.region(['us-east1'])
  ///- functions.region(['us-east1', 'us-central1'])
  FunctionsBuilder region(List<String> regions) {
    // return FunctionsBuilder.getInstance(jsObject.region(jsifyList(regions)));
    return FunctionsBuilder.getInstance(
        callMethod(jsObject, 'region', regions));
  }

  ///Configure runtime options for the function.
  FunctionsBuilder runWith(RuntimeOptions options) =>
      FunctionsBuilder.getInstance(jsObject.runWith(options));
}

///Interface for firebase functions
class FunctionsModuleBuilder extends FunctionsBuilder {
  FunctionsModuleBuilder._fromJsObject(interop.FunctionsBuilderJsImpl jsObject)
      : super._fromJsObject(jsObject);

  ///Cloud Functions max timeout value.
  num get maxTimeoutSeconds => interop.MAX_TIMEOUT_SECONDS;

  ///Cloud Functions min timeout value.
  num get minTimeoutSeconds => interop.MIN_TIMEOUT_SECONDS;

  ///List of all regions supported by Cloud Functions.
  List<String> get supportedRegions => List.from(interop.SUPPORTED_REGIONS);

  ///List of available memory options supported by Cloud Functions.
  List<String> get validMemoryOptions =>
      List.from(interop.VALID_MEMORY_OPTIONS);

  /// Store and retrieve project configuration data such as third-party API
  /// keys or other settings. You can set configuration values using the
  /// Firebase CLI.
  Map<String, dynamic> config() => dartify(interop.config());
}

/// Resource is a standard format for defining a resource
/// (google.rpc.context.AttributeContext.Resource). In Cloud Functions, it is
/// the resource that triggered the function - such as a storage bucket.
class Resource extends JsObjectWrapper<interop.ResourceJsImpl> {
  static final _expando = Expando<Resource>();

  /// Resource is a standard format for defining a resource
  /// (google.rpc.context.AttributeContext.Resource). In Cloud Functions, it is
  /// the resource that triggered the function - such as a storage bucket.
  static Resource getInstance(interop.ResourceJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Resource._fromJsObject(jsObject);
  }

  Resource._fromJsObject(interop.ResourceJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, String> get labels =>
      dartify(jsObject.labels)?.cast<String, String>();

  String get name => jsObject.name;

  String get service => jsObject.service;

  String get type => jsObject.type;
}
